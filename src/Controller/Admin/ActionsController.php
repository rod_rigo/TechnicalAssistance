<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Actions Controller
 *
 * @property \App\Model\Table\ActionsTable $Actions
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user)){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Actions->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getActions(){
        $data = $this->Actions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->order(['order_position' => 'ASC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getActionsDeleted(){
        $data = $this->Actions->find('all',['withDeleted'])
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->whereNotNull('Actions.deleted')
            ->order(['order_position' => 'ASC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function view($id = null){
        $action = $this->Actions->get($id, [
            'contain' => [],
        ]);
        $this->set(compact('action'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $action = $this->Actions->newEmptyEntity();
        $orderPosition = @$this->Actions->find()->max('order_position')->order_position;
        if ($this->request->is('post')) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            $action->user_id = $this->Auth->user('id');
            $action->order_position = intval($orderPosition) + 1;
            if ($this->Actions->save($action)) {
                $result = ['message' => ucwords('The action has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($action->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $action->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $action = $this->Actions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            $action->user_id = $this->Auth->user('id');
            if ($this->Actions->save($action)) {
                $result = ['message' => ucwords('The action has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($action->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $action->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($action));
    }

    public function orderPosition(){
        $this->request->allowMethod(['post']);
        if ($this->request->is(['post'])) {
            $connection = ConnectionManager::get('default');

            $connection->begin();

            try{

                foreach ($this->request->getData() as $key => $request){
                    $this->Actions->updateAll([
                        'order_position' => $request['order_position']
                    ],[
                        'id =' => $request['id']
                    ]);
                }

                $connection->commit();
                $result = ['message' => ucwords('The order position has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));

            }catch (\Exception $exception){
                $connection->rollback();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $action = $this->Actions->get($id);

        $taken = TableRegistry::getTableLocator()->get('Takens')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($taken)){
            $result = ['message' => ucwords('The action type has been constrained to takens'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        $attribute = TableRegistry::getTableLocator()->get('Attributes')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($attribute)){
            $result = ['message' => ucwords('The action type has been constrained to attributes'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        $aspect = TableRegistry::getTableLocator()->get('Aspects')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($aspect)){
            $result = ['message' => ucwords('The action type has been constrained to aspects'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Actions->delete($action)) {
            $result = ['message' => ucwords('The action has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The action has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $action = $this->Actions->get($id,[
            'withDeleted'
        ]);
        if ($this->Actions->restore($action)) {
            $result = ['message' => ucwords('The action has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The action has not been restored'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $action = $this->Actions->get($id,[
            'withDeleted'
        ]);

        $taken = TableRegistry::getTableLocator()->get('Takens')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($taken)){
            $result = ['message' => ucwords('The action type has been constrained to takens'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        $attribute = TableRegistry::getTableLocator()->get('Attributes')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($attribute)){
            $result = ['message' => ucwords('The action type has been constrained to attributes'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        $aspect = TableRegistry::getTableLocator()->get('Aspects')->find()
            ->where([
                'action_id =' => intval($action->id)
            ])
            ->last();

        if(!empty($aspect)){
            $result = ['message' => ucwords('The action type has been constrained to aspects'), 'result' => ucwords('info')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

        if ($this->Actions->hardDelete($action)) {
            $result = ['message' => ucwords('The action has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The action has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
