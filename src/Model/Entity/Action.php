<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Action Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $action
 * @property int $is_active
 * @property int $is_others
 * @property int $order_position
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Taken[] $takens
 * @property \App\Model\Entity\Attribute[] $attributes
 * @property \App\Model\Entity\Aspect[] $aspects
 */
class Action extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'action' => true,
        'is_active' => true,
        'is_others' => true,
        'order_position' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'takens' => true,
        'attributes' => true,
        'aspects' => true,
    ];

    public function _setAction($value){
        return strtoupper($value);
    }

}
