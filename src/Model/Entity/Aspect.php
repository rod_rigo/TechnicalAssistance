<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Aspect Entity
 *
 * @property int $id
 * @property int $assistance_id
 * @property int $action_id
 * @property int $attribute_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Assistance $assistance
 * @property \App\Model\Entity\Action $action
 * @property \App\Model\Entity\SubType $sub_type
 */
class Aspect extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assistance_id' => true,
        'action_id' => true,
        'attribute_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'assistance' => true,
        'action' => true,
        'sub_type' => true,
    ];
}
