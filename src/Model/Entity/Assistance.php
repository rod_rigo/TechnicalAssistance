<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assistance Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $position
 * @property int $account_type_id
 * @property int $establishment_id
 * @property int $department_id
 * @property int $account_id
 * @property int $request_id
 * @property string $deped_email
 * @property string $personal_email
 * @property string $permanent_contact_no
 * @property string $description
 * @property string|null $other_details
 * @property string $file_before
 * @property string $file_after
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\AccountType $account_type
 * @property \App\Model\Entity\Establishment $establishment
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\Element[] $elements
 * @property \App\Model\Entity\Finding[] $findings
 * @property \App\Model\Entity\Taken[] $takens
 * @property \App\Model\Entity\Aspect[] $aspects
 */
class Assistance extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'position' => true,
        'account_type_id' => true,
        'establishment_id' => true,
        'department_id' => true,
        'account_id' => true,
        'request_id' => true,
        'deped_email' => true,
        'personal_email' => true,
        'permanent_contact_no' => true,
        'description' => true,
        'other_details' => true,
        'file_before' => true,
        'file_after' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'account_type' => true,
        'establishment' => true,
        'department' => true,
        'account' => true,
        'request' => true,
        'elements' => true,
        'findings' => true,
        'takens' => true,
        'aspects' => true,
    ];

    protected function _setFirstName($value){
        return ucwords($value);
    }

    protected function _setMiddleName($value){
        return ucwords($value);
    }

    protected function _setLastName($value){
        return ucwords($value);
    }

    protected function _setPosition($value){
        return ucwords($value);
    }

    protected function _setDescription($value){
        return ucwords($value);
    }

    protected function _setOtherDetails($value){
        return ucwords($value);
    }

}
