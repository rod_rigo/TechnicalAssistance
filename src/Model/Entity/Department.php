<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Department Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $establishment_id
 * @property string $department
 * @property int $is_active
 *  @property int $is_for_school
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Establishment $establishment
 * @property \App\Model\Entity\Head $head
 * @property \App\Model\Entity\Assistance[] $assistances
 */
class Department extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'establishment_id' => true,
        'department' => true,
        'is_active' => true,
        'is_for_school' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'establishment' => true,
        'head' => true,
        'assistances' => true,
    ];

    protected function _setDepartment($value){
        return ucwords($value);
    }

}
