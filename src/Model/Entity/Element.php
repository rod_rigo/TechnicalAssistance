<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Element Entity
 *
 * @property int $id
 * @property int $assistance_id
 * @property int $nature_id
 * @property int $sub_type_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Assistance $assistance
 * @property \App\Model\Entity\Nature $nature
 * @property \App\Model\Entity\SubType $sub_type
 */
class Element extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assistance_id' => true,
        'nature_id' => true,
        'sub_type_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'assistance' => true,
        'nature' => true,
        'sub_type' => true,
    ];
}
