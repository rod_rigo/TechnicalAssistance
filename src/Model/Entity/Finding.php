<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Finding Entity
 *
 * @property int $id
 * @property int $assistance_id
 * @property string|null $item_description
 * @property string|null $serial_no
 * @property string|null $problem_issue
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Assistance $assistance
 */
class Finding extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'assistance_id' => true,
        'item_description' => true,
        'serial_no' => true,
        'problem_issue' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'assistance' => true,
    ];

    protected function _setItemDescription($value){
        return ucwords($value);
    }

    protected function _setProblemIssue($value){
        return ucwords($value);
    }

}
