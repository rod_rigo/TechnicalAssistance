<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Head Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $primary_id
 * @property int $department_id
 * @property int $cluster_id
 * @property string $head
 * @property string $head_contact_no
 * @property string $school_id
 * @property string $ict_coordinator
 * @property string $ict_contact_no
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Primary $primary
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Cluster $cluster
 */
class Head extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'primary_id' => true,
        'department_id' => true,
        'cluster_id' => true,
        'head' => true,
        'head_contact_no' => true,
        'school_id' => true,
        'ict_coordinator' => true,
        'ict_contact_no' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'primary' => true,
        'department' => true,
        'cluster' => true,
    ];

    protected function _setHead($value){
        return ucwords($value);
    }

    protected function _setIctCoordinator($value){
        return ucwords($value);
    }

}
