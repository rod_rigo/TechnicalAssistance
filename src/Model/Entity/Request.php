<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Request Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $account_id
 * @property string $request
 * @property int $is_active
 * @property int $is_default
 * @property int $order_position
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Assistance[] $assistances
 */
class Request extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'account_id' => true,
        'request' => true,
        'is_active' => true,
        'is_default' => true,
        'order_position' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'account' => true,
        'assistances' => true,
    ];

    protected function _setRequest($value){
        return ucwords($value);
    }

}
