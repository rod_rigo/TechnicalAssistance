<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\AccountType[] $account_types
 * @property \App\Model\Entity\Account[] $accounts
 * @property \App\Model\Entity\Action[] $actions
 * @property \App\Model\Entity\Attribute[] $attributes
 * @property \App\Model\Entity\Cluster[] $clusters
 * @property \App\Model\Entity\Department[] $departments
 * @property \App\Model\Entity\Establishment[] $establishments
 * @property \App\Model\Entity\Head[] $heads
 * @property \App\Model\Entity\Layout[] $layouts
 * @property \App\Model\Entity\Nature[] $natures
 * @property \App\Model\Entity\Primary[] $primaries
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\SubType[] $sub_types
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'account_types' => true,
        'accounts' => true,
        'actions' => true,
        'attributes' => true,
        'clusters' => true,
        'departments' => true,
        'establishments' => true,
        'heads' => true,
        'layouts' => true,
        'natures' => true,
        'primaries' => true,
        'requests' => true,
        'sub_types' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword($value){
        return (new DefaultPasswordHasher())->hash($value);
    }

}
