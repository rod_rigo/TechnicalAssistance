<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Assistances Model
 *
 * @property \App\Model\Table\EstablishmentsTable&\Cake\ORM\Association\BelongsTo $Establishments
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\BelongsTo $Requests
 * @property \App\Model\Table\ElementsTable&\Cake\ORM\Association\HasMany $Elements
 * @property \App\Model\Table\FindingsTable&\Cake\ORM\Association\HasMany $Findings
 * @property \App\Model\Table\TakensTable&\Cake\ORM\Association\HasMany $Takens
 *
 * @method \App\Model\Entity\Assistance newEmptyEntity()
 * @method \App\Model\Entity\Assistance newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Assistance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Assistance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Assistance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Assistance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Assistance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Assistance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assistance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assistance[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Assistance[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Assistance[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Assistance[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssistancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('assistances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AccountTypes', [
            'foreignKey' => 'account_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Establishments', [
            'foreignKey' => 'establishment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Elements', [
            'foreignKey' => 'assistance_id',
        ])->setDependent(true);
        $this->hasMany('Findings', [
            'foreignKey' => 'assistance_id',
        ])->setDependent(true);
        $this->hasMany('Takens', [
            'foreignKey' => 'assistance_id',
        ])->setDependent(true);
        $this->hasMany('Aspects', [
            'foreignKey' => 'assistance_id',
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', true)
            ->notEmptyString('first_name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 255)
            ->requirePresence('middle_name', true)
            ->notEmptyString('middle_name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', true)
            ->notEmptyString('last_name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('position')
            ->maxLength('position', 255)
            ->requirePresence('position', true)
            ->notEmptyString('position', ucwords('please fill out this field'),false);

        $validator
            ->scalar('deped_email')
            ->email('deped_email')
            ->maxLength('deped_email', 255)
            ->requirePresence('deped_email', true)
            ->notEmptyString('deped_email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('personal_email')
            ->email('personal_email')
            ->maxLength('personal_email', 255)
            ->requirePresence('personal_email', true)
            ->notEmptyString('personal_email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('permanent_contact_no')
            ->maxLength('permanent_contact_no', 255)
            ->requirePresence('permanent_contact_no', true)
            ->notEmptyString('permanent_contact_no', ucwords('please fill out this field'),false)
            ->add('permanent_contact_no','permanent_contact_no', [
                'rule' => function($value) {
                    $regex = '/^(09|\+639)([0-9]{9})$/';

                    if(!preg_match_all($regex, $value)){
                        return ucwords('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please fill out this field'),false);

        $validator
            ->scalar('other_details')
            ->maxLength('other_details', 4294967295)
            ->allowEmptyString('other_details');

        $validator
            ->scalar('file_before')
            ->maxLength('file_before', 255)
            ->requirePresence('file_before', true)
            ->notEmptyString('file_before', ucwords('please fill out this field'),false);

        $validator
            ->scalar('file_after')
            ->maxLength('file_after', 255)
            ->requirePresence('file_after', true)
            ->notEmptyString('file_after', ucwords('please fill out this field'),false);


        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['account_type_id'], 'AccountTypes'), ['errorField' => 'account_type_id']);
        $rules->add($rules->existsIn(['establishment_id'], 'Establishments'), ['errorField' => 'establishment_id']);
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);
        $rules->add($rules->existsIn(['account_id'], 'Accounts'), ['errorField' => 'account_id']);
        $rules->add($rules->existsIn(['request_id'], 'Requests'), ['errorField' => 'request_id']);

        return $rules;
    }
}
