<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Heads Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PrimariesTable&\Cake\ORM\Association\BelongsTo $Primaries
 * @property \App\Model\Table\ClustersTable&\Cake\ORM\Association\BelongsTo $Clusters
 * @property \App\Model\Table\SchoolsTable&\Cake\ORM\Association\BelongsTo $Schools
 *
 * @method \App\Model\Entity\Head newEmptyEntity()
 * @method \App\Model\Entity\Head newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Head[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Head get($primaryKey, $options = [])
 * @method \App\Model\Entity\Head findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Head patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Head[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Head|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Head saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Head[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Head[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Head[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Head[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HeadsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('heads');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Primaries', [
            'foreignKey' => 'primary_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Clusters', [
            'foreignKey' => 'cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('head')
            ->maxLength('head', 255)
            ->requirePresence('head', true)
            ->notEmptyString('head', ucwords('please fill out this field'),false);

        $validator
            ->scalar('head_contact_no')
            ->maxLength('head_contact_no', 255)
            ->requirePresence('head_contact_no', true)
            ->notEmptyString('head_contact_no', ucwords('please fill out this field'),false);

        $validator
            ->scalar('ict_coordinator')
            ->maxLength('ict_coordinator', 255)
            ->requirePresence('ict_coordinator', true)
            ->notEmptyString('ict_coordinator', ucwords('please fill out this field'),false);

        $validator
            ->scalar('ict_contact_no')
            ->maxLength('ict_contact_no', 255)
            ->requirePresence('ict_contact_no', true)
            ->notEmptyString('ict_contact_no', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['primary_id'], 'Primaries'), ['errorField' => 'primary_id']);
        $rules->add($rules->existsIn(['cluster_id'], 'Clusters'), ['errorField' => 'cluster_id']);
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);
        $rules->add($rules->isUnique(['department_id'],ucwords('this school has already head')), ['errorField' => 'department_id']);
        $rules->add($rules->isUnique(['school_id'],ucwords('this school id is already exists')), ['errorField' => 'school_id']);

        return $rules;
    }
}
