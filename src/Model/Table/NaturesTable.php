<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Natures Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ElementsTable&\Cake\ORM\Association\HasMany $Elements
 * @property \App\Model\Table\SubTypesTable&\Cake\ORM\Association\HasMany $SubTypes
 *
 * @method \App\Model\Entity\Nature newEmptyEntity()
 * @method \App\Model\Entity\Nature newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Nature[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nature get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nature findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Nature patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nature[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nature|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nature saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nature[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Nature[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Nature[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Nature[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NaturesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('natures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Elements', [
            'foreignKey' => 'nature_id',
        ]);
        $this->hasMany('SubTypes', [
            'foreignKey' => 'nature_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nature')
            ->maxLength('nature', 255)
            ->requirePresence('nature', true)
            ->notEmptyString('nature', ucwords('please fill out this field'),false);

        $validator
            ->numeric('is_active')
            ->requirePresence('is_active', true)
            ->notEmptyString('is_active', ucwords('please select active value'), false)
            ->add('is_active','is_active',[
                'rule'=> function($value){
                    $isActive = [0, 1];

                    if(!in_array(intval($value), $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('order_position')
            ->requirePresence('order_position', true)
            ->notEmptyString('order_position', ucwords('please select order position value'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['nature'], ucwords('this nature is already exists')), ['errorField' => 'nature']);

        return $rules;
    }
}
