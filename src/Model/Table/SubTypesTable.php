<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * SubTypes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\NaturesTable&\Cake\ORM\Association\BelongsTo $Natures
 * @property \App\Model\Table\ElementsTable&\Cake\ORM\Association\HasMany $Elements
 *
 * @method \App\Model\Entity\SubType newEmptyEntity()
 * @method \App\Model\Entity\SubType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SubType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SubType get($primaryKey, $options = [])
 * @method \App\Model\Entity\SubType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SubType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SubType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SubType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SubType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SubType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SubType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SubTypesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sub_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Natures', [
            'foreignKey' => 'nature_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Elements', [
            'foreignKey' => 'sub_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('nature_id')
            ->requirePresence('nature_id', true)
            ->notEmptyString('nature_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('sub_type')
            ->maxLength('sub_type', 255)
            ->requirePresence('sub_type', true)
            ->notEmptyString('sub_type', ucwords('please fill out this field'), false);

        $validator
            ->numeric('is_active')
            ->requirePresence('is_active', true)
            ->notEmptyString('is_active', ucwords('please select active value'), false)
            ->add('is_active','is_active',[
                'rule'=> function($value){
                    $isActive = [0, 1];

                    if(!in_array(intval($value), $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('order_position')
            ->requirePresence('order_position', true)
            ->notEmptyString('order_position', ucwords('please select order position value'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['nature_id'], 'Natures'), ['errorField' => 'nature_id']);
        $rules->add($rules->isUnique(['nature_id', 'sub_type'],ucwords('this sub type is already exists in this nature')), ['errorField' => 'sub_type']);

        return $rules;
    }
}
