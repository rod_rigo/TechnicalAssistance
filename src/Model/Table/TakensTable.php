<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Takens Model
 *
 * @property \App\Model\Table\AssistancesTable&\Cake\ORM\Association\BelongsTo $Assistances
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\BelongsTo $Actions
 *
 * @method \App\Model\Entity\Taken newEmptyEntity()
 * @method \App\Model\Entity\Taken newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Taken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Taken get($primaryKey, $options = [])
 * @method \App\Model\Entity\Taken findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Taken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Taken[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Taken|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Taken saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Taken[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Taken[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Taken[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Taken[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TakensTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('takens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Assistances', [
            'foreignKey' => 'assistance_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Actions', [
            'foreignKey' => 'action_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['assistance_id'], 'Assistances'), ['errorField' => 'assistance_id']);
        $rules->add($rules->existsIn(['action_id'], 'Actions'), ['errorField' => 'action_id']);

        return $rules;
    }
}
