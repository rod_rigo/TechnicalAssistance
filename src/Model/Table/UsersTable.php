<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\AccountTypesTable&\Cake\ORM\Association\HasMany $AccountTypes
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\HasMany $Accounts
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\AttributesTable&\Cake\ORM\Association\HasMany $Attributes
 * @property \App\Model\Table\ClustersTable&\Cake\ORM\Association\HasMany $Clusters
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\HasMany $Departments
 * @property \App\Model\Table\EstablishmentsTable&\Cake\ORM\Association\HasMany $Establishments
 * @property \App\Model\Table\HeadsTable&\Cake\ORM\Association\HasMany $Heads
 * @property \App\Model\Table\LayoutsTable&\Cake\ORM\Association\HasMany $Layouts
 * @property \App\Model\Table\NaturesTable&\Cake\ORM\Association\HasMany $Natures
 * @property \App\Model\Table\PrimariesTable&\Cake\ORM\Association\HasMany $Primaries
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\SubTypesTable&\Cake\ORM\Association\HasMany $SubTypes
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('AccountTypes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Accounts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Actions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Attributes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Clusters', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Departments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Establishments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Heads', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Layouts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Natures', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Primaries', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('SubTypes', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('username')
            ->maxLength('username', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('username', true)
            ->notEmptyString('username', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->maxLength('name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('password', 'create')
            ->notEmptyString('password', ucwords('please fill out this field'),true);

        $validator
            ->scalar('token')
            ->maxLength('token', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('token', true)
            ->notEmptyString('token', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query->where([
            'OR' => [ //<-- we use OR operator for our SQL
                'username' => $options['username'], //<-- username column
                'email' => $options['username'] //<-- email column
            ]], [], true);

        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}
