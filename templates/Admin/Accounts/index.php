<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('account', ucwords('account'))?>
                        <?=$this->Form->text('account',[
                            'class' => 'form-control',
                            'id' => 'account',
                            'required' => true,
                            'placeholder' => ucwords('account'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('default',[
                                'id' => 'default',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => false,
                            ])?>
                            <?=$this->Form->label('default', ucwords('Default'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => 1
                ]);?>
                <?= $this->Form->hidden('is_default',[
                    'id' => 'is-default',
                    'value' => 0
                ]);?>
                <?= $this->Form->hidden('order_position',[
                    'id' => 'order-position',
                    'value' => rand(1,9)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Account">
            New Account
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Account</th>
                        <th>Is Active</th>
                        <th>Is Default</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/accounts/index')?>

