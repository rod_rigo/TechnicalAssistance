<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Nature[]|\Cake\Collection\CollectionInterface $accounts
 */
?>

<script>
    var id = parseInt(<?=intval($account->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Accounts', 'action' => 'index'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="Return">
            New Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Nature Form</h3>
            </div>
            <?= $this->Form->create($account,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('account', ucwords('account'))?>
                        <?=$this->Form->text('account',[
                            'class' => 'form-control',
                            'id' => 'account',
                            'required' => true,
                            'placeholder' => ucwords('account'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($account->is_active),
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('default',[
                                'id' => 'default',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($account->is_default),
                            ])?>
                            <?=$this->Form->label('default', ucwords('Default'))?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($account->is_active)
                ]);?>
                <?= $this->Form->hidden('is_default',[
                    'id' => 'is-default',
                    'value' => intval($account->is_default)
                ]);?>
                <?= $this->Form->hidden('order_position',[
                    'id' => 'order-position',
                    'value' => intval($account->order_position)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0 m-1',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0 m-1',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Request</th>
                        <th>Is Active</th>
                        <th>Is Default</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/accounts/view')?>
