<?php
/**
 * @var \App\View\AppView $this
 */?>
<script>
    var id = parseInt(<?=intval($action->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Actions', 'action' => 'index'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="Return">
            New Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Action Form</h3>
            </div>
            <?= $this->Form->create($action,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('action', ucwords('action'))?>
                        <?=$this->Form->text('action',[
                            'class' => 'form-control',
                            'id' => 'action',
                            'required' => true,
                            'placeholder' => ucwords('action'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($action->is_active),
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('others',[
                                'id' => 'others',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($action->is_others),
                            ])?>
                            <?=$this->Form->label('others', ucwords('others'))?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($action->is_active)
                ]);?>
                <?= $this->Form->hidden('order_position',[
                    'id' => 'order-position',
                    'value' => intval($action->order_position)
                ]);?>
                <?= $this->Form->hidden('is_others',[
                    'id' => 'is-others',
                    'value' => intval($action->is_others)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0 m-1',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0 m-1',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Attributes</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/actions/view')?>