<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?=$this->Html->css('admin/assistances/index')?>

<?=$this->Form->create(null,['id' => 'form', 'class' => 'row', 'type' => 'file'])?>
    <div class="col-sm-12 col-md-4 col-lg-4 mt-1">
        <?=$this->Form->label('start_date', ucwords('start date'))?>
        <?=$this->Form->date('start_date',[
            'class' => 'form-control',
            'id' => 'start-date',
            'required' => true,
            'title' => ucwords('please fill out this field')
        ])?>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4 mt-1">
        <?=$this->Form->label('end_date', ucwords('end date'))?>
        <?=$this->Form->date('end_date',[
            'class' => 'form-control',
            'id' => 'end-date',
            'required' => true,
            'title' => ucwords('please fill out this field')
        ])?>
    </div>
    <div class="col-sm-12 col-md-2 col-lg-2 mt-1">
        <?=$this->Form->label('records', ucwords('records'))?>
        <?=$this->Form->number('records',[
            'class' => 'form-control',
            'id' => 'records',
            'required' => true,
            'title' => ucwords('please fill out this field'),
            'min' => 10000,
            'value' => 10000
        ])?>
    </div>
    <div class="col-sm-12 col-md-2 col-lg-2 mt-1 d-flex justify-content-start align-items-end">
        <?=$this->Form->button('Search',[
            'type' => 'submit',
            'class' => 'btn btn-primary rounded-0'
        ])?>
    </div>
<?=$this->Form->end()?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Establishment</th>
                            <th>Department</th>
                            <th>Account Type</th>
                            <th>For Account</th>
                            <th>Request</th>
                            <th>Email (DepEd)</th>
                            <th>Email (Personal)</th>
                            <th>Contact No (Personal)</th>
                            <th>Description</th>
                            <th>Other Details</th>
                            <th>Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/assistances/index')?>

