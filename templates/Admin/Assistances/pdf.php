<?php

use Dompdf\Dompdf;
use Dompdf\Options;


$path = WWW_ROOT. 'img'. DS. $layout->header;
$folder = new \Cake\Filesystem\File($path);
$header = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==';
if($folder->exists()){
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $header = 'data:'.($mime).';base64,'.($image);
}

$path = WWW_ROOT. 'img'. DS. $layout->footer;
$folder = new \Cake\Filesystem\File($path);
$footer = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==';
if($folder->exists()){
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $footer = 'data:'.($mime).';base64,'.($image);
}

$path = WWW_ROOT.'img'.DS.'checked.png';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$checked = 'data:'.($mime).';base64,'.($image);

$path = WWW_ROOT.'img'.DS.'uncheck.png';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$uncheck = 'data:'.($mime).';base64,'.($image);

// Setup Dompdf
$options = new Options();
$options->set('isPhpEnabled', true);
$options->set('isRemoteEnabled', true);
$options->set('isHtml5ParserEnabled', true);

$created = strtotime($assistance->created);
$date = date('m/d/Y', $created);
$time = date('h:i:s A', $created);

$accountLists = '';
foreach ($accounts as $key => $value){
    if(intval($key) == intval($assistance->account_id)){
        $accountLists .= '<img src="'.($checked).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 2px; font-weight: normal; font-size: 12px;">'.($value).'</span>';
    }else{
        $accountLists .= '<img src="'.($uncheck).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 2px; font-weight: normal; font-size: 12px;">'.($value).'</span>';
    }
}

$accountTypeLists = '';
foreach ($accountTypes as $key => $value){
    if(intval($key) == intval($assistance->account_type_id)){
        $accountTypeLists .= '<img src="'.($checked).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 2px; font-weight: normal; font-size: 12px;">'.($value).'</span>';
    }else{
        $accountTypeLists .= '<img src="'.($uncheck).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 2px; font-weight: normal; font-size: 12px;">'.($value).'</span>';
    }
}

$requestLists = '';
foreach ($requests as $key => $value){
    if(intval($key) == intval($assistance->request_id)){
        $requestLists .= '<img src="'.($checked).'" style="height: 9px; width: 9px;margin-left: 1px;margin-right: 0.5px;"><span style="margin-right: 1px; font-weight: normal; font-size: 9px;">'.($value).'</span>';
    }else{
        $requestLists .= '<img src="'.($uncheck).'" style="height: 9px; width: 9px;margin-left: 1px;margin-right: 0.5px;"><span style="margin-right: 1px; font-weight: normal; font-size: 9px;">'.($value).'</span>';
    }

}

$tables = '';
$no = 0;
$widths = [strval('38%'), strval('30%'), strval('22%'), strval('9.5%')];
foreach ($natures as $key => $nature){
    $tables .= '<div style="width: '.($widths[intval($key)]).'; height: 100%; float: left; position:relative; border-right: 1px solid black;">
        <div style="border-bottom: 1px solid black; height: 20%;">
           <p style="text-align: center; text-decoration: underline; font-size: 10px; line-height: 14px; font-weight: bold; text-transform: capitalize;">
            '.($nature->nature).'
            </p>
        </div>
        <div style="height: 80%; width: 100%;">';

    if(count($nature->sub_types) > 3){
        $sub_type = $nature->sub_types;

        $tables.= '<div style="width: 50%; text-align: left; height: 100%; border-right: 1px solid black; position: relative; float: left;">';
        for ($i = 0; $i < 3; $i++ ){
           try{
               $no++;
                if(in_array(intval(@$sub_type[intval($i)]->id), array_column($elements, 'sub_type_id'))){
                    $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%; text-decoration: underline">'.($no.'. '.@$sub_type[intval($i)]->sub_type).'</p>';
                }else{
                    $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%;">'.($no.'. '.@$sub_type[intval($i)]->sub_type).'</p>';
                }
           }catch (\Exception $exception){

            }
        }
        $tables .= '</div>';

        $tables.= '<div style="width: 50%; text-align: left; height: 100%; position: relative; float: right;">';
        for ($f = 3; $f < 6; $f++ ){
            try{
                $no++;
                if(in_array(intval(@$sub_type[intval($f)]->id), array_column($elements, 'sub_type_id'))){
                    $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%; text-decoration: underline">'.($no.'. '.@$sub_type[intval($f)]->sub_type).'</p>';
                }else{
                    $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%;">'.($no.'. '.@$sub_type[intval($f)]->sub_type).'</p>';
                }
            }catch (\Exception $exception){

            }
        }
        $tables .= '</div>';

    }else{
        $tables.= '<div style="width: 100%; text-align: left; height: 100%;">';
        foreach ($nature->sub_types as $sub_type){
            $no++;
            if(in_array(intval($sub_type->id), array_column($elements, 'sub_type_id'))){
                $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%; text-decoration: underline">'.($no.'. '.@$sub_type->sub_type).'</p>';
            }else{
                $tables .= '<p style="text-align: left; font-size: 11px; text-transform: capitalize; padding: 0; margin: 2px; text-indent: 3px; min-width: 100%;">'.($no.'. '.@$sub_type->sub_type).'</p>';
            }
        }
        $tables .= '</div>';
    }

    $tables.='</div> </div>';
}

$findings = '';
$finding = $assistance->findings;
for($h = 0; $h < 3; $h++){
    $border = (intval($h) == intval(2))? '': 'border-bottom: 1px solid black;';
    $findings .= '<tr style="margin: 0; padding: 0;width: 100%;">';
    $findings .='<td style="'.($border).' width: 33.30%; height: 24%; position: relative; float: left;">
                  <p style="; margin-top: 0; font-size: 10px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                    '.(ucwords(@$finding[$h]->item_description)).'
                </p>
            </td>';

    $findings .='<td style="'.($border).' border-right: 1px solid black; border-left: 1px solid black; width: 33.30%; height: 24%; position: relative; float: left;">
                  <p style="; margin-top: 0; font-size: 10px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                     '.(ucwords(@$finding[$h]->serial_no)).'
                </p>
            </td>';

    $findings .='<td style="'.($border).' width: 33.30%; height: 24%; position: relative; float: left;">
                  <p style="; margin-top: 0; font-size: 10px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                     '.(ucwords(@$finding[$h]->problem_issue)).'
                </p>
            </td>';
    $findings .= '</tr>';
}

$actionLists = '';
$actionCounter = intval(1);

foreach ($actions->toArray() as $key => $action){

    $attributes = '';
     foreach ($action['attributes'] as $attribute){
         if(in_array(intval($attribute['id']),array_column($aspects,'attribute_id'))){
             $attributes .= '<u>'.($attribute['attribute']).'</u>/';
         }else{
             $attributes .= ($attribute['attribute']).'/';
         }
     }

    $text = $action['action'].' '.($attributes);
    $text = substr($text,0, (intval(strlen($text)) - intval(1)));

    if(intval(strlen($text)) > intval(70)){
        $actionLists .= '<br>';
    }elseif (intval($actionCounter) % intval(3) === 0){
        $actionLists .= '<br>';
    }

    if(in_array(intval($action['id']), array_column($takens,'action_id'))){
        $actionLists .= '<img src="'.($checked).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 20px; font-size: 10px; font-weight: bold; text-transform: capitalize;">'.($text).'</span>';
    }else{
        $actionLists .= '<img src="'.($uncheck).'" style="height: 10px; width: 10px;margin-left: 1px;margin-right: 1px;"><span style="margin-right: 20px; font-size: 10px; font-weight: bold; text-transform: capitalize;">'.($text).'</span>';
    }

    $actionCounter++;
}

// HTML content for the certificate
$html = '<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>';
$html .= '<style>
    *{
    margin: 0;
    padding: 0;
}
.logo-img{
    height: 180px;
    width: 100%;
    object-fit: contain;
}
body{
    padding: 18px 18px;
}
</style> ';

$html .= '<div style="text-align: center;width: 100%;">
<img src="' . ($header) . '" class="logo-img" alt="DepEd Logo">
</div>
<div style="border-bottom: 2px solid black; width: 100%; margin-top: 2px;"></div>';

$html.= '<p style="text-transform: uppercase; width: 100%; font-size: 14px;margin-top: 2px; text-align: center; font-weight: bold;">ICT TECHNICAL ASSISTANCE (TA) FORM</p>';

$html .= '<div style="width: 100%; border: 1px solid black; height: 52em; padding: 0;">';

$html .='<div style="width: 100%; border-bottom: 1px solid black; height: 19%;">
    <div style="border-right: 1px solid black;  width: 50%; height: 97.5%; float: left; padding: 3px;">
        <p style="text-transform: uppercase; width: 100%; font-size: 14px;margin-top: 2px; text-align: center; font-weight: bold;">Client Information</p>
        
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">First Name: '.($assistance->first_name).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Last Name: '.($assistance->last_name).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Plantilla Position: '.($assistance->position).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Office/School: '.($assistance->department->department).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Date Of Request: '.($date).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Time Of Request: '.($time).'</p>
        
    </div>
     <div style="width: 50%; height: 97.5%; float: left; padding: 3px; padding-left: 5px;">
        <p style="text-transform: uppercase; width: 100%; font-size: 14px;margin-top: 2px; text-align: left; font-weight: bold; text-indent: 5px;">
            For Account: '.($accountLists).'
        </p>
        
         <p style="text-transform: uppercase; width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">
            Request: '.($requestLists).'
        </p>
        <p style="text-transform: uppercase; width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">
            Account Type: '.($accountTypeLists).'
        </p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Middle Name: '.($assistance->middle_name).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">DepEd Email: '.($assistance->deped_email).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 5px;">Recovery Information</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 25px;">Personal E-Mail: '.($assistance->personal_email).'</p>
        <p style="width: 100%; font-size: 14px;margin-top: 2px; text-align: left; text-indent: 25px;">Permanent Mobile No: '.($assistance->permanent_contact_no).'</p>
    </div>
</div>';

$html .='<div style="width: 100%; border-bottom: 1px solid black; height: 9%; margin: 0; padding: 0;">
    <table style="border: none; width: 100%; height: 99.8%;margin: 0; margin-top: 1px; padding: 0; position: relative; border-collapse: collapse;">
        <tr>
            <th style="text-align: left; font-weight: bolder; width: 100%; height: 15%; text-transform: uppercase; text-indent: 5px; font-size: 12px;">
                For Schools:
            </th>
            <th style="text-align: left; font-weight: bolder; width: 100%; height: 15%; text-transform: uppercase; text-indent: 5px; font-size: 12px;">
              
            </th>
        </tr>
        <tr>
            <th style="text-align: left; width: 50%; height: 24.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                District/Cluster: '.(@$department->head->cluster->cluster).'
            </th>
           <th style="text-align: left; width: 50%; height: 24.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                School ID: '.(@$department->head->school_id).'
            </th>
        </tr>
        <tr>
            <th style="text-align: left; width: 50%; height: 24.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                School Head: '.(@$department->head->head).'
            </th>
           <th style="text-align: left; width: 50%; height: 24.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                ICT Coordinator: '.(@$department->head->ict_coordinator).'
           </th>
        </tr>
       
         <tr>
            <th style="text-align: left; width: 50%; height: 25.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                Contact No: '.(@$department->head->head_contact_no).'
            </th>
           <th style="text-align: left; width: 50%; height: 25.5%; text-transform: capitalize; text-indent: 5px; font-size: 12px;">
                Contact No: '.(@$department->head->ict_contact_no).'
           </th>
        </tr>
    </table>
</div>';

$html .='<div style="width: 100%; border-bottom: 1px solid black; height: 9%;">
    <p style="width: 100%; height: 25%; font-size: 14px; font-weight: bold; font-style: italic; margin-top: 4px; text-indent: 8px;">Short Description of your Request/Problems Encountered:</p>
     <p style="min-width: 100%; height: 75%; font-size: 12px; margin-top: 4px; text-indent: 8px;">'.($assistance->description).'</p>
</div>';

$html .='<div style="width: 100%; border: 1px solid black; height: 11.5%; margin-top: 0; padding-bottom: 1px;">
    <div style="margin-top: 0; position: relative; height: 15%; background: #5a9bd4; width: 100%;font-weight: bolder; border-bottom: 1px solid black; text-align: center; font-size: 12px;">
        --for The ICT Unit--
    </div>
    <div style="width: 10%; height: 85%; position: relative; text-transform: uppercase; text-align: left; font-weight: bold; border-right: 1px solid black; font-size: 14px;word-spacing: 25px; line-height: 25px; padding-left: 8px;">
        Nature Of Request
    </div>
     <div style="width: 89%; height: 85%; position: relative; top: -85%; left: 11%; margin: 0; padding: 0;">
        '.($tables).'
    </div>
</div>';

$html .='<div style="width: 100%; border-bottom: 1px solid black; height: 15%;">
    <div style="width: 10%; height: 100%; position: relative; text-transform: uppercase; text-align: left; font-weight: bold; border-right: 1px solid black; font-size: 14px; padding-left: 8px;">
        <p style="transform: rotate(-90deg); float: right; margin-top: 58px; margin-right: 10px;">Findings</p>
    </div>
    <div style="width: 89%; height: 100%; position: relative; top: -100%; left: 11.3%; margin: 0;">
        
        <table style="width: 99.5%; border-collapse: collapse; height: 100%;margin: 0; padding: 0; position: relative; top:0; border:none;">
            <tr style="margin: 0; padding: 0;width: 100%;">
              <td style="border-bottom: 1px solid black; border-bottom: 1px solid black; width: 33.30%; height: 25%; position: relative; float: left; padding: 0;">
                 <p style="margin-top: 0; font-size: 14px; font-weight: bolder; text-transform: uppercase; text-align: center;">
                    Item Description
                </p>
                <p style="margin-top: 0; font-size: 8px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                    (Property Number)
                </p>
            </td>
            <td style="border-bottom: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width: 33.30%; height: 25%; position: relative; float: left;">
                 <p style="margin-top: 0; font-size: 14px; font-weight: bolder; text-transform: uppercase; text-align: center;">
                    Serial No.
                </p>
                <p style="margin-top: 0; font-size: 8px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                    (Please Specify)
                </p>
            </td>
             <td style="border-bottom: 1px solid black; border-bottom: 1px solid black; width: 33.30%; height: 25%; position: relative; float: left;">
                <p style="margin-top: 0; font-size: 14px; font-weight: bolder; text-transform: uppercase; text-align: center;">
                    Problem/Issue
                </p>
                <p style="; margin-top: 0; font-size: 8px; font-weight: bolder; text-transform: capitalize; text-align: center;">
                    (Please Specify)
                </p>
            </td>
        </tr>
            '.($findings).'
        </table>
        
    </div>
</div>';

$html .='<div style="width: 100%; border-bottom: 1px solid black; height: 15%;">
 <div style="width: 10%; height: 100%; position: relative; text-transform: uppercase; text-align: left; font-weight: bold; border-right: 1px solid black; font-size: 14px; padding-left: 8px;">
        <p style="transform: rotate(-90deg); float: right; margin-top: 42px; margin-right: 10px;">Action Taken</p>
    </div>
    <div style="width: 89%; height: 100%; position: relative; top: -100%; left: 11.3%; margin: 0;">
        
        <div style="width: 99.5%; height: 100%;margin: 0; padding: 0; position: relative; top:0; border:none;">
            <div style="height: 55%; width: 100%; padding: 1px; margin: 0;">
                '.($actionLists).'
            </div>
              <div style="height: 44%; width: 100%; padding: 1px; margin: 0; font-size: 12px; position: relative; top: -10%;">
                <span style="font-weight: bold; font-style: italic;">Other Details (If Any):</span><br>
                '.($assistance->other_details).'
            </div>
        </div>
        
    </div>
</div>';

$html .='<div style="width: 100%; height: 15%;">
        <table style="border: none; width: 100%; height: 99.8%;margin: 0; margin-top: 1px; padding: 0; position: relative; border-collapse: collapse;">
            <tr>
                <th style="text-align: left; font-weight: bolder; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px; border-right: 1px solid black;">
                    (Division Office) Chief/Unit Head
                </th>
                <th style="text-align: left; font-weight: bolder; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px;">
                    Noted/Processed By:
                </th>
            </tr>
             <tr>
                <th style="text-align: left; font-weight: bold; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px; border-right: 1px solid black;">
                    (School) School Head/Representative:
                </th>
                <th style="text-align: left; font-weight: bold; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px;"></th>
            </tr>
            
             <tr>
                <th style="text-align: left; width: 50%; height: 55%; text-transform: capitalize; font-size: 12px; border-right: 1px solid black; padding-left: 4px; padding-right: 4px;">
                    <p style="font-size: 12px; margin: 0; position: relative; bottom: -18%; text-align: center; font-weight: bolder;text-transform: uppercase;">'.(@$department->head->head).'</p>
                    <p style="font-size: 10px; margin: 0; text-transform: capitalize; position: relative; bottom: -20%; text-align: center; border-bottom: 2px solid black;"></p>
                    <p style="font-size: 10px; margin: 0; text-transform: capitalize; position: relative; bottom: -22%; text-align: center;">Signature Over Printed Name</p>
                </th>
                <th style="text-align: left; width: 50%; height: 55%; text-transform: capitalize; font-size: 12px; padding-left: 4px; padding-right: 4px;">
                <p style="font-size: 12px; margin: 0; position: relative; bottom: -18%; text-align: center; font-weight: bolder;text-transform: uppercase;">'.(strtoupper($layout->name)).'</p>
                    <p style="font-size: 10px; margin: 0; text-transform: capitalize; position: relative; bottom: -20%; text-align: center; border-bottom: 2px solid black;"></p>
                    <p style="font-size: 10px; margin: 0; text-transform: capitalize; position: relative; bottom: -22%; text-align: center;">'.(strtoupper($layout->position)).'</p>
                </th>
            </tr>
            
             <tr>
                <th style="text-align: left; font-weight: bold; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px; border-right: 1px solid black;">Date:</th>
                <th style="text-align: left; font-weight: bold; width: 50%; height: 15%; text-transform: capitalize; text-indent: 8px; font-size: 12px;">Date:</th>
            </tr>
        </table>
</div>';

$html .= '</div>';

$html .= '<img src="'.($footer).'" style="width: 100%; position:absolute; bottom: 0.5%; height: 80px; left:0; object-fit:contain;">';

$html .= '</body>';
$html .='</html>';

// Instantiate Dompdf
$dompdf = new Dompdf($options);

// Load HTML content
$dompdf->loadHtml($html);

//$dompdf->setPaper([0, 0, 828/2, 585], 'landscape');
$dompdf->setPaper('A4');

// Render PDF (DOMPDF)
$dompdf->render();

// Output PDF to browser
$dompdf->stream(uniqid().'.pdf', ['Attachment' => false]);

exit(0);
?>
