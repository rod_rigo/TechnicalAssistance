<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?=$this->Html->css('admin/assistances/today')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Establishment</th>
                        <th>Department</th>
                        <th>Account Type</th>
                        <th>For Account</th>
                        <th>Request</th>
                        <th>Email (DepEd)</th>
                        <th>Email (Personal)</th>
                        <th>Contact No (Personal)</th>
                        <th>Description</th>
                        <th>Other Details</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/assistances/today')?>

