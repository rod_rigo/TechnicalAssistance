<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assistance|\Cake\Collection\CollectionInterface $assistance
 */
?>

<script>
    var id = parseInt(<?=intval($assistance->id)?>);
</script>

<div class="row">
   <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
       <button type="button" id="pdf" class="btn btn-primary rounded-0" title="PDF">
           PDF
       </button>
   </div>
</div>

<?=$this->Form->create($assistance,['id' => 'form', 'class' => 'row', 'type' => 'file'])?>
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card">
            <div class="card-body">
                <h5 class="mb-1">Client Information</h5>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('first_name', ucwords('first name'))?>
                        <?=$this->Form->text('first_name',[
                            'id' => 'first-name',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('first name'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('middle_name', ucwords('middle name'))?>
                        <?=$this->Form->text('middle_name',[
                            'id' => 'middle-name',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('middle name'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('last_name', ucwords('last name'))?>
                        <?=$this->Form->text('last_name',[
                            'id' => 'last-name',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('last name'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('deped_email', ucwords('DepEd Email'))?>
                        <?=$this->Form->email('deped_email',[
                            'id' => 'deped-email',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('DepEd Email'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('personal_email', ucwords('personal Email'))?>
                        <?=$this->Form->email('personal_email',[
                            'id' => 'personal-email',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('personal Email'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('permanent_contact_no', ucwords('permanent contact no'))?>
                        <?=$this->Form->text('permanent_contact_no',[
                            'id' => 'permanent-contact-no',
                            'class' => 'form-control',
                            'required' => true,
                            'placeholder' => ucwords('permanent contact no'),
                            'pattern' => '(09|\+639)([0-9]{9})',
                            'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('position', ucwords('position'))?>
                        <?=$this->Form->text('position',[
                            'id' => 'position',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('position'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                </div>

                <h5 class="mb-1">Account Type</h5>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group d-flex flex-column justify-content-start align-items-start">
                            <?php foreach ($accountTypes as $key => $value):?>
                                <div class="icheck-primary m-1">
                                    <?=$this->Form->checkbox('account_types.'.($key),[
                                        'id' => 'account-types-'.($key),
                                        'hiddenField' => false,
                                        'class' => 'account-types',
                                        'value' => $key,
                                        'required' => (intval($key) == intval($assistance->account_type_id))? true: false,
                                        'checked' => (intval($key) == intval($assistance->account_type_id))? true: false
                                    ])?>
                                    <?=$this->Form->label('account_types.'.($key),ucwords($value))?>
                                </div>
                            <?php endforeach;?>
                            <?=$this->Form->hidden('account_type_id',[
                                'id' => 'account-type-id',
                                'required' => true,
                                'value' => intval($assistance->account_type_id)
                            ])?>
                            <small></small>
                        </div>
                    </div>
                </div>

                <h5 class="mb-1">Office/School</h5>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group d-flex flex-column justify-content-start align-items-start">
                            <?php foreach ($establishments as $key => $value):?>
                                <div class="icheck-primary m-1">
                                    <?=$this->Form->checkbox('establishments.'.($key),[
                                        'id' => 'establishments-'.($key),
                                        'hiddenField' => false,
                                        'class' => 'establishments',
                                        'value' => $key,
                                        'required' => (intval($key) == intval($assistance->establishment_id))? true: false,
                                        'checked' => (intval($key) == intval($assistance->establishment_id))? true: false
                                    ])?>
                                    <?=$this->Form->label('establishments.'.($key),ucwords($value))?>
                                </div>
                            <?php endforeach;?>
                            <?=$this->Form->hidden('establishment_id',[
                                'id' => 'establishment-id',
                                'required' => true,
                                'value' => intval($assistance->establishment_id)
                            ])?>
                            <small></small>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('department_id', ucwords('department'))?>
                        <?=$this->Form->select('department_id', $departments,[
                            'id' => 'department-id',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Choose Establishment'),
                        ])?>
                        <small></small>
                    </div>
                </div>

                <h5 class="mb-1">For Account</h5>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group d-flex flex-column justify-content-start align-items-start">
                            <?php foreach ($accounts as $key => $value):?>
                                <div class="icheck-primary m-1">
                                    <?=$this->Form->checkbox('accounts.'.($key),[
                                        'id' => 'accounts-'.($key),
                                        'hiddenField' => false,
                                        'class' => 'accounts',
                                        'value' => $key,
                                        'required' => (intval($assistance->account_id) == intval($key))? true: false,
                                        'checked' => (intval($assistance->account_id) == intval($key))? true: false,
                                    ])?>
                                    <?=$this->Form->label('accounts.'.($key),ucwords($value))?>
                                </div>
                            <?php endforeach;?>
                            <?=$this->Form->hidden('account_id',[
                                'id' => 'account-id',
                                'required' => true,
                                'value' => intval($assistance->account_id)
                            ])?>
                            <small></small>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('request_id', ucwords('request'))?>
                        <?=$this->Form->select('request_id', $requests,[
                            'id' => 'request-id',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Choose Account'),
                        ])?>
                        <small></small>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('description', ucwords('description'))?>
                        <?=$this->Form->textarea('description',[
                            'id' => 'description',
                            'class' => 'form-control',
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('Short Description of your Request/Problems Encountered'),
                            'pattern' => '(.){1,}'
                        ])?>
                        <small></small>
                    </div>
                </div>

                <?php if(count($assistance->findings) > intval(0)):?>
                    <h5 class="mb-1">Findings</h5>
                    <div class="row mb-3">
                        <?php foreach ($assistance->findings as $key => $finding):?>
                            <?=$this->Form->hidden('findings.'.($key).'.id',[
                                'required' => true,
                                'id' => 'findings-'.($key).'-id',
                                'value' => intval($finding->id)
                            ])?>
                            <?=$this->Form->hidden('findings.'.($key).'.assistance_id',[
                                'required' => true,
                                'id' => 'findings-'.($key).'-assistance-id',
                                'value' => intval($finding->assistance_id)
                            ])?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                                <?=$this->Form->label('findings.'.($key).'.item_description', ucwords('item description'))?>
                                <?=$this->Form->text('findings.'.($key).'.item_description',[
                                    'id' => 'findings-'.($key).'-item-description',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('item description'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 mb-3 mt-2">
                                <?=$this->Form->label('findings.'.($key).'.serial_no', ucwords('serial no'))?>
                                <?=$this->Form->text('findings.'.($key).'.serial_no',[
                                    'id' => 'findings-'.($key).'-serial-no',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('serial no'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 mb-3 mt-2">
                                <?=$this->Form->label('findings.'.($key).'.problem_issue', ucwords('problem issue'))?>
                                <?=$this->Form->text('findings.'.($key).'.problem_issue',[
                                    'id' => 'findings-'.($key).'-problem-issue',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('problem issue'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php else:?>
                    <h5 class="mb-1">Findings</h5>
                    <div class="row mb-3">
                        <?php for($i = 0; $i < 3; $i++):?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                                <?=$this->Form->label('findings.'.($i).'.item_description', ucwords('item description'))?>
                                <?=$this->Form->text('findings.'.($i).'.item_description',[
                                    'id' => 'findings-'.($i).'-item-description',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('item description'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 mb-3 mt-2">
                                <?=$this->Form->label('findings.'.($i).'.serial_no', ucwords('serial no'))?>
                                <?=$this->Form->text('findings.'.($i).'.serial_no',[
                                    'id' => 'findings-'.($i).'-serial-no',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('serial no'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 mb-3 mt-2">
                                <?=$this->Form->label('findings.'.($i).'.problem_issue', ucwords('problem issue'))?>
                                <?=$this->Form->text('findings.'.($i).'.problem_issue',[
                                    'id' => 'findings-'.($i).'-problem-issue',
                                    'class' => 'form-control',
                                    'required' => false,
                                    'title' => ucwords('this field is optional'),
                                    'placeholder' => ucwords('problem issue'),
                                    'pattern' => '(.){1,}',
                                ])?>
                                <small></small>
                            </div>
                        <?php endfor;?>
                    </div>
                <?php endif;?>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('file_before',[
                    'id' => 'file-before',
                    'required' => true,
                    'value' => $assistance->file_before
                ])?>
                <?=$this->Form->hidden('file_after',[
                    'id' => 'file-after',
                    'required' => true,
                    'value' => $assistance->file_after
                ])?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>

    </div>
<?=$this->Form->end()?>

<div class="row">
    <?=$this->Form->create($taken,['id' => 'taken-form', 'class' => 'col-sm-12 col-md-5 col-lg-5 mt-3', 'type' => 'file'])?>
        <div class="card">
            <div class="card-body">
                <h5 class="mb-1">Actions</h5>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <ul class="list-unstyled">
                            <?php foreach ($actions as $action):?>
                                <li class="my-3">
                                    <div class="icheck-primary m-1">
                                        <?=$this->Form->checkbox('actions[]',[
                                            'id' => 'actions-'.($action->id),
                                            'hiddenField' => false,
                                            'class' => 'actions',
                                            'value' => $action->id,
                                            'required' => (count($assistance->takens) > intval(0))? false: true,
                                            'checked' => (in_array(intval($action->id), array_column($takens,'action_id')))? true: false
                                        ])?>
                                        <?=$this->Form->label('actions.'.($action->id), ucwords($action->action))?>
                                    </div>
                                    <?php if(count($action->attributes) > intval(0)):?>
                                        <ul style="list-style: none;">
                                            <?php foreach ($action->attributes as $key => $attribute):?>
                                                <li>
                                                    <div class="icheck-primary m-1">
                                                        <?=$this->Form->checkbox('attributes[]',[
                                                            'id' => 'attributes-'.($attribute->id),
                                                            'hiddenField' => false,
                                                            'class' => 'attributes',
                                                            'value' => $attribute->id,
                                                            'checked' => (in_array(intval($attribute->id), array_column($aspects,'attribute_id')))
                                                        ])?>
                                                        <?=$this->Form->label('attributes.'.($attribute->id), strtoupper($attribute->attribute))?>
                                                    </div>
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                </li>
                            <?php endforeach;?>
                        </ul>
                        <?=$this->Form->hidden('assistance_id',[
                            'id' => 'assistance-id',
                            'required' => true,
                            'value' => intval($assistance->id)
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('other_details', ucwords('other details'))?>
                        <?=$this->Form->textarea('other_details',[
                            'id' => 'other-details',
                            'class' => 'form-control',
                            'required' => false,
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('other details'),
                            'pattern' => '(.){1,}',
                            'value' => ucwords($assistance->other_details)
                        ])?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
    <?=$this->Form->end()?>
    <?=$this->Form->create($element,['id' => 'element-form', 'class' => 'col-sm-12 col-md-7 col-lg-7 mt-3', 'type' => 'file'])?>
    <div class="card">
        <div class="card-body">

            <?php $counter = intval(0);?>

            <h5 class="mb-2">Elements</h5>

            <div class="row mb-1">
               <div class="col-sm-12 col-md-12 col-lg-12">
                   <ul class="list-unstyled">
                       <?php foreach ($natures as $nature):?>
                           <li>
                               <?=strtoupper($nature->nature)?>
                               <ul style="list-style: none;">
                                   <?php foreach ($nature->sub_types as $sub_type):?>
                                       <li>
                                           <div class="icheck-primary m-1">
                                               <?=$this->Form->checkbox('sub_types[]',[
                                                   'id' => 'sub-types-'.($counter),
                                                   'hiddenField' => false,
                                                   'class' => 'sub-types',
                                                   'value' => $sub_type->id,
                                                   'required' => (count($assistance->elements) > intval(0))? false: true,
                                                   'checked' => (in_array(intval($sub_type->id), array_column($elements,'sub_type_id')))? true: false
                                               ])?>
                                               <?=$this->Form->label('sub_types.'.($counter),ucwords($sub_type->sub_type))?>
                                           </div>
                                       </li>
                                       <?php $counter++?>
                                   <?php endforeach;?>
                               </ul>
                           </li>
                       <?php endforeach;?>
                   </ul>
                   <?=$this->Form->hidden('assistance_id',[
                       'id' => 'assistance-id',
                       'required' => true,
                       'value' => intval($assistance->id)
                   ])?>
                   <small></small>
               </div>
            </div>

        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?= $this->Form->button(__('Submit'),[
                'class' => 'btn btn-success rounded-0',
                'title' => ucwords('Submit')
            ])?>
        </div>
    </div>
    <?=$this->Form->end()?>
</div>

<div class="row mt-3">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-file-before-tab" data-toggle="pill" href="#custom-tabs-file-before" role="tab" aria-controls="custom-tabs-file-before" aria-selected="true">
                            File(Before)
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-after-file-tab" data-toggle="pill" href="#custom-tabs-after-file" role="tab" aria-controls="custom-tabs-after-file" aria-selected="false">
                            File(After)
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-file-before" role="tabpanel" aria-labelledby="custom-tabs-file-before-tab">
                        <div class="row">
                            <?php
                                $path = WWW_ROOT. 'files'. DS. ($assistance->file_before);
                                $folder = new \Cake\Filesystem\File($path);
                            ?>
                            <?=$this->Form->create($assistance,['type' => 'file', 'id' => 'file-before-form', 'class' => 'col-sm-12 col-md-12 col-lg-12'])?>
                                <div class="form-group">
                                    <?=$this->Form->label('file_before_file', ucwords('File Before'))?>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <?=$this->Form->file('file',[
                                                'id' => 'file-before-file',
                                                'class' => 'custom-file-input',
                                                'required' => true,
                                                'accept' => 'application/pdf'
                                            ])?>
                                            <?=$this->Form->label('file_before_file', ucwords('Choose File'),[
                                                'class' => 'custom-file-label',
                                                'id' => 'file-before-label'
                                            ])?>
                                        </div>
                                        <div class="input-group-append bg-info">
                                            <?=$this->Form->button('Upload',[
                                                'class' => 'btn btn-primary btn-block input-group-text bg-info rounded-0',
                                                'type' => 'submit',
                                                'escapeTitle' => false
                                            ])?>
                                        </div>
                                    </div>
                                </div>
                            <?=$this->Form->end()?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                <embed src="<?=$this->Url->assetUrl('/files/'.($assistance->file_before))?>" loading="lazy" id="file-before-embed" width="800" height="<?=($folder->exists())? strval('800'): strval('0');?>">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-after-file" role="tabpanel" aria-labelledby="custom-tabs-after-file-tab">
                        <div class="row">
                            <?php
                            $path = WWW_ROOT. 'files'. DS. ($assistance->file_after);
                            $folder = new \Cake\Filesystem\File($path);
                            ?>
                            <?=$this->Form->create($assistance,['type' => 'file', 'id' => 'file-after-form', 'class' => 'col-sm-12 col-md-12 col-lg-12'])?>
                            <div class="form-group">
                                <?=$this->Form->label('file_after_file', ucwords('File After'))?>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <?=$this->Form->file('file',[
                                            'id' => 'file-after-file',
                                            'class' => 'custom-file-input',
                                            'required' => true,
                                            'accept' => 'application/pdf'
                                        ])?>
                                        <?=$this->Form->label('file_after_file', ucwords('Choose File'),[
                                            'class' => 'custom-file-label',
                                            'id' => 'file-after-label'
                                        ])?>
                                    </div>
                                    <div class="input-group-append bg-info">
                                        <?=$this->Form->button('Upload',[
                                            'class' => 'btn btn-primary btn-block input-group-text bg-info rounded-0',
                                            'type' => 'submit',
                                            'escapeTitle' => false
                                        ])?>
                                    </div>
                                </div>
                            </div>
                            <?=$this->Form->end()?>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                <embed src="<?=$this->Url->assetUrl('/files/'.($assistance->file_after))?>" loading="lazy" id="file-after-embed" width="800" height="<?=($folder->exists())? strval('800'): strval('0');?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

<?=$this->Html->script('admin/assistances/view')?>