<?php
/**
 * @var \App\View\AppView $this
 */
?>

    <div class="row">

        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                    <i class="fa fa-home"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Departments</span>
                    <span class="info-box-number" id="departments">0</span>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                    <i class="fa fa-user-cog"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Accounts</span>
                    <span class="info-box-number" id="accounts">0</span>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                    <i class="fa fa-file"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Requests</span>
                    <span class="info-box-number" id="requests">0</span>
                </div>
            </div>
        </div>

    </div>

    <h5 class="mb-2">Assistances</h5>
    <div class="row">

        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                    <i class="fas fa-book"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Today</span>
                    <span class="info-box-number" id="total-assistances-today">0</span>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                    <i class="fas fa-book"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Week</span>
                    <span class="info-box-number" id="total-assistances-week">0</span>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                <i class="fas fa-book"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Month</span>
                    <span class="info-box-number" id="total-assistances-month">0</span>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="info-box">
                <span class="info-box-icon bg-info">
                  <i class="fas fa-book"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Year</span>
                    <span class="info-box-number" id="total-assistances-year">0</span>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-5 mb-2">
            <?=$this->Form->year('year',[
                'class' => 'form-control',
                'id' => 'year',
                'value' => date('Y'),
                'min' => 2000
            ])?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <div class="card p-3">
                <canvas id="assistance-chart-month" width="20" height="20" style="height: 35em;"></canvas>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 mb-2">
            <div class="card p-3">
                <canvas id="assistance-department-chart" width="20" height="20" style="height: 35em;"></canvas>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 mb-2">
            <div class="card p-3">
                <canvas id="assistance-account-chart" width="20" height="20" style="height: 35em;"></canvas>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-4 my-2">
            <?=$this->Form->select('account', $accounts,[
                'id' => 'account',
                'class' => 'form-control',
                'empty' => ucwords('Account')
            ])?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <div class="card p-3">
                <canvas id="assistance-request-chart" width="20" height="20" style="height: 40em;"></canvas>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-4 my-2">
            <?=$this->Form->select('nature', $natures,[
                'id' => 'nature',
                'class' => 'form-control',
                'empty' => ucwords('Nature')
            ])?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <div class="card p-3">
                <canvas id="assistance-sub-type-chart" width="20" height="20" style="height: 40em;"></canvas>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-4 my-2">
            <?=$this->Form->select('action', $actions,[
                'id' => 'action',
                'class' => 'form-control',
                'empty' => ucwords('Action')
            ])?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <div class="card p-3">
                <canvas id="assistance-attribute-chart" width="20" height="20" style="height: 40em;"></canvas>
            </div>
        </div>
    </div>

<?=$this->Html->script('admin/dashboards/index')?>