<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Head[]|\Cake\Collection\CollectionInterface $heads
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-xl">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('school_id', ucwords('School ID'))?>
                        <?=$this->Form->text('school_id',[
                            'class' => 'form-control',
                            'id' => 'school-id',
                            'required' => true,
                            'placeholder' => ucwords('School ID'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('primary_id', ucwords('primary'))?>
                        <?=$this->Form->select('primary_id', $primaries,[
                            'class' => 'form-control',
                            'id' => 'primary-id',
                            'required' => true,
                            'empty' => ucwords('please select primary'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('cluster_id', ucwords('cluster'))?>
                        <?=$this->Form->select('cluster_id', $clusters,[
                            'class' => 'form-control',
                            'id' => 'cluster-id',
                            'required' => true,
                            'empty' => ucwords('please select cluster'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('department_id', ucwords('department'))?>
                        <?=$this->Form->select('department_id', $departments,[
                            'class' => 'form-control',
                            'id' => 'department-id',
                            'required' => true,
                            'empty' => ucwords('please select department'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('head', ucwords('head'))?>
                        <?=$this->Form->text('head',[
                            'class' => 'form-control',
                            'id' => 'head',
                            'required' => true,
                            'placeholder' => ucwords('head'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('head_contact_no', ucwords('head (Contact No)'))?>
                        <?=$this->Form->text('head_contact_no',[
                            'class' => 'form-control',
                            'id' => 'head-contact-no',
                            'required' => true,
                            'placeholder' => ucwords('head (Contact No)'),
                            'pattern' => '(09|\+639)([0-9]{9})',
                            'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('ict_coordinator', ucwords('ict coordinator'))?>
                        <?=$this->Form->text('ict_coordinator',[
                            'class' => 'form-control',
                            'id' => 'ict-coordinator',
                            'required' => true,
                            'placeholder' => ucwords('ict coordinator'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('ict_contact_no', ucwords('Ict (Contact No)'))?>
                        <?=$this->Form->text('ict_contact_no',[
                            'class' => 'form-control',
                            'id' => 'ict-contact-no',
                            'required' => true,
                            'placeholder' => ucwords('ict (Contact No)'),
                            'pattern' => '(09|\+639)([0-9]{9})',
                            'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                        ])?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Head">
            New Head
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>School ID</th>
                        <th>Primary</th>
                        <th>Cluster</th>
                        <th>Department</th>
                        <th>Head</th>
                        <th>Head (Contact No)</th>
                        <th>Ict Coordinator</th>
                        <th>Ict Coordinator (Contact No)</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/heads/index')?>


