<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Layout[]|\Cake\Collection\CollectionInterface $layouts
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a type="button" href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'add'])?>" class="btn btn-primary rounded-0" title="New Layout">
            New Layout
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Header</th>
                        <th>Footer</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/layouts/index')?>
