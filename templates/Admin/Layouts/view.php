<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Layout $layout
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

    <script>
        var id = parseInt(<?=intval($layout->id)?>);
    </script>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                            <img src="#" id="image-preview" class="w-100" alt="" height="500" width="500" style="object-fit: contain !important;">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-end align-items-center">
                    <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Layout Form</h3>
                </div>
                <?= $this->Form->create($layout,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->label('name', ucwords('name'))?>
                            <?=$this->Form->text('name',[
                                'class' => 'form-control',
                                'id' => 'name',
                                'required' => true,
                                'placeholder' => ucwords('name'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->label('position', ucwords('position'))?>
                            <?=$this->Form->text('position',[
                                'class' => 'form-control',
                                'id' => 'position',
                                'required' => true,
                                'placeholder' => ucwords('position'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                            <div class="row">
                                <small><span class="text-danger">*</span>Click The Eye Icon Before Submitting</small>
                                <div class="col-sm-12 col-md-10 col-lg-11">
                                    <img src="<?=$this->Url->assetUrl('/img/'.($layout->header))?>" class="img-thumbnail" id="header-cropper" alt="Logo Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-1 d-sm-flex flex-sm-row flex-sm-wrap justify-content-sm-center align-items-sm-center d-md-flex flex-md-column justify-content-md-around align-items-md-center mt-2">
                                    <button type="button" id="header-crop" class="btn btn-primary rounded-0" title="Crop">
                                        <i class="fa fa-crop"></i>
                                    </button>
                                    <button type="button" id="header-move" class="btn btn-primary rounded-0" title="Move">
                                        <i class="fa fa-arrows-alt"></i>
                                    </button>

                                    <button type="button" id="header-zoom-in" class="btn btn-primary rounded-0" title="Zoom In">
                                        <i class="fa fa-search-plus"></i>
                                    </button>
                                    <button type="button" id="header-zoom-out" class="btn btn-primary rounded-0" title="Zoom Out">
                                        <i class="fa fa-search-minus"></i>
                                    </button>

                                    <button type="button" id="header-rotate-left" class="btn btn-primary rounded-0" title="Rotate Left">
                                        <i class="fa fa-undo"></i>
                                    </button>
                                    <button type="button" id="header-rotate-right" class="btn btn-primary rounded-0" title="Rotate Right">
                                        <i class="fa fa-redo"></i>
                                    </button>

                                    <button type="button" id="header-flip-vertical" class="btn btn-primary rounded-0" title="Flip Vertical">
                                        <i class="fa fa-retweet fa-rotate-90"></i>
                                    </button>
                                    <button type="button" id="header-flip-horizontal" class="btn btn-primary rounded-0" title="Flip Horizontal">
                                        <i class="fa fa-retweet"></i>
                                    </button>

                                    <button type="button" id="header-view" class="btn btn-primary rounded-0" title="Flip Horizontal">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                                    <?=$this->Form->label('header_height', ucwords('height (Header)'))?>
                                    <?=$this->Form->number('header_height',[
                                        'class' => 'form-control',
                                        'id' => 'header-height',
                                        'required' => true,
                                        'placeholder' => ucwords('height'),
                                        'pattern' => '(.){1,}',
                                        'title' => ucwords('Please Fill Out This Field'),
                                        'min' => intval(1080),
                                        'value' => intval(4096)
                                    ])?>
                                    <small></small>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                                    <?=$this->Form->label('header_width', ucwords('width (Header)'))?>
                                    <?=$this->Form->number('header_width',[
                                        'class' => 'form-control',
                                        'id' => 'header-width',
                                        'required' => true,
                                        'placeholder' => ucwords('width'),
                                        'pattern' => '(.){1,}',
                                        'title' => ucwords('Please Fill Out This Field'),
                                        'min' => intval(1080),
                                        'value' => intval(4096)
                                    ])?>
                                    <small></small>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end mt-2">
                                    <button type="button" id="header-move-up" class="btn btn-primary rounded-0 mt-1" title="Move Up">
                                        <i class="fa fa-arrow-circle-up"></i>
                                    </button>
                                    <button type="button" id="header-move-down" class="btn btn-primary rounded-0" title="Move Down">
                                        <i class="fa fa-arrow-circle-down"></i>
                                    </button>
                                    <button type="button" id="header-move-left" class="btn btn-primary rounded-0" title="Move Left">
                                        <i class="fa fa-arrow-circle-left"></i>
                                    </button>
                                    <button type="button" id="header-move-right" class="btn btn-primary rounded-0 mt-1" title="Move Down">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                                    <div class="form-group">
                                        <?=$this->Form->label('header_file', ucwords('header'))?>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <?=$this->Form->file('header_file',[
                                                    'class' => 'custom-file-input rounded-0',
                                                    'id' => 'header-file',
                                                    'accept' => 'image/*',
                                                    'required' => false
                                                ])?>
                                                <?=$this->Form->label('header_file', ucwords('Choose file'),[
                                                    'class' => 'custom-file-label rounded-0'
                                                ])?>
                                            </div>
                                        </div>
                                        <small></small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                            <div class="row">
                                <small><span class="text-danger">*</span>Click The Eye Icon Before Submitting</small>
                                <div class="col-sm-12 col-md-10 col-lg-11">
                                    <img src="<?=$this->Url->assetUrl('/img/'.($layout->footer))?>" class="img-thumbnail" id="footer-cropper" alt="Logo Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-1 d-sm-flex flex-sm-row flex-sm-wrap justify-content-sm-center align-items-sm-center d-md-flex flex-md-column justify-content-md-around align-items-md-center mt-2">
                                    <button type="button" id="footer-crop" class="btn btn-primary rounded-0" title="Crop">
                                        <i class="fa fa-crop"></i>
                                    </button>
                                    <button type="button" id="footer-move" class="btn btn-primary rounded-0" title="Move">
                                        <i class="fa fa-arrows-alt"></i>
                                    </button>

                                    <button type="button" id="footer-zoom-in" class="btn btn-primary rounded-0" title="Zoom In">
                                        <i class="fa fa-search-plus"></i>
                                    </button>
                                    <button type="button" id="footer-zoom-out" class="btn btn-primary rounded-0" title="Zoom Out">
                                        <i class="fa fa-search-minus"></i>
                                    </button>

                                    <button type="button" id="footer-rotate-left" class="btn btn-primary rounded-0" title="Rotate Left">
                                        <i class="fa fa-undo"></i>
                                    </button>
                                    <button type="button" id="footer-rotate-right" class="btn btn-primary rounded-0" title="Rotate Right">
                                        <i class="fa fa-redo"></i>
                                    </button>

                                    <button type="button" id="footer-flip-vertical" class="btn btn-primary rounded-0" title="Flip Vertical">
                                        <i class="fa fa-retweet fa-rotate-90"></i>
                                    </button>
                                    <button type="button" id="footer-flip-horizontal" class="btn btn-primary rounded-0" title="Flip Horizontal">
                                        <i class="fa fa-retweet"></i>
                                    </button>

                                    <button type="button" id="footer-view" class="btn btn-primary rounded-0" title="Flip Horizontal">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                                    <?=$this->Form->label('footer_height', ucwords('height (Footer)'))?>
                                    <?=$this->Form->number('footer_height',[
                                        'class' => 'form-control',
                                        'id' => 'footer-height',
                                        'required' => true,
                                        'placeholder' => ucwords('height'),
                                        'pattern' => '(.){1,}',
                                        'title' => ucwords('Please Fill Out This Field'),
                                        'min' => intval(1080),
                                        'value' => intval(4096)
                                    ])?>
                                    <small></small>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                                    <?=$this->Form->label('footer_width', ucwords('width (Footer)'))?>
                                    <?=$this->Form->number('footer_width',[
                                        'class' => 'form-control',
                                        'id' => 'footer-width',
                                        'required' => true,
                                        'placeholder' => ucwords('width'),
                                        'pattern' => '(.){1,}',
                                        'title' => ucwords('Please Fill Out This Field'),
                                        'min' => intval(1080),
                                        'value' => intval(4096)
                                    ])?>
                                    <small></small>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end mt-2">
                                    <button type="button" id="footer-move-up" class="btn btn-primary rounded-0 mt-1" title="Move Up">
                                        <i class="fa fa-arrow-circle-up"></i>
                                    </button>
                                    <button type="button" id="footer-move-down" class="btn btn-primary rounded-0" title="Move Down">
                                        <i class="fa fa-arrow-circle-down"></i>
                                    </button>
                                    <button type="button" id="footer-move-left" class="btn btn-primary rounded-0" title="Move Left">
                                        <i class="fa fa-arrow-circle-left"></i>
                                    </button>
                                    <button type="button" id="footer-move-right" class="btn btn-primary rounded-0 mt-1" title="Move Down">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                                    <div class="form-group">
                                        <?=$this->Form->label('footer_file', ucwords('footer'))?>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <?=$this->Form->file('footer_file',[
                                                    'class' => 'custom-file-input rounded-0',
                                                    'id' => 'footer-file',
                                                    'accept' => 'image/*',
                                                    'required' => false
                                                ])?>
                                                <?=$this->Form->label('header_file', ucwords('Choose file'),[
                                                    'class' => 'custom-file-label rounded-0'
                                                ])?>
                                            </div>
                                        </div>
                                        <small></small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="icheck-primary d-inline">
                                <?=$this->Form->checkbox('active',[
                                    'id' => 'active',
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => true,
                                ])?>
                                <?=$this->Form->label('active', ucwords('Active'))?>
                            </div>
                            <small></small>
                        </div>

                    </div>

                </div>

                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?= $this->Form->hidden('user_id',[
                        'id' => 'user-id',
                        'value' => intval(@$auth['id'])
                    ]);?>
                    <?= $this->Form->hidden('is_active',[
                        'id' => 'is-active',
                        'value' => intval(1),
                        'required' => true
                    ]);?>
                    <?= $this->Form->hidden('header',[
                        'id' => 'header',
                        'required' => true,
                        'value' => $layout->header
                    ]);?>
                    <?= $this->Form->hidden('footer',[
                        'id' => 'footer',
                        'required' => true,
                        'value' => $layout->footer
                    ]);?>
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                        Return
                    </a>
                    <?= $this->Form->button(__('Submit'),[
                        'class' => 'btn btn-success rounded-0',
                        'title' => ucwords('Submit')
                    ])?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

<?=$this->Html->script('admin/layouts/view')?>