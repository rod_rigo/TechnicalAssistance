<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Nature[]|\Cake\Collection\CollectionInterface $natures
 */
?>

<script>
    var id = parseInt(<?=intval($nature->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'SubTypes', 'action' => 'index'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="Return">
            New Return
        </a>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Nature Form</h3>
            </div>
            <?= $this->Form->create($nature,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('nature', ucwords('nature'))?>
                        <?=$this->Form->text('nature',[
                            'class' => 'form-control',
                            'id' => 'nature',
                            'required' => true,
                            'placeholder' => ucwords('nature'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => strtoupper($nature->nature)
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($nature->is_active),
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($nature->is_active)
                ]);?>
                <?= $this->Form->hidden('order_position',[
                    'id' => 'order-position',
                    'value' => intval($nature->order_position)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0 m-1',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0 m-1',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Sub Type</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/natures/view')?>


