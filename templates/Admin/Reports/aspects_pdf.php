<?php

use Dompdf\Dompdf;
use Dompdf\Options;

$path = WWW_ROOT. 'img'. DS. $layout->header;
$folder = new \Cake\Filesystem\File($path);
$header = '';
if($folder->exists()){
    $header = WWW_ROOT. 'img'. DS. $layout->header;
}

$path = WWW_ROOT. 'img'. DS. $layout->footer;
$folder = new \Cake\Filesystem\File($path);
$footer = '';
if($folder->exists()){
    $footer = WWW_ROOT. 'img'. DS. $layout->footer;
}


// Initialize Dompdf with some options
$options = new Options();
$options->set('isHtml5ParserEnabled', true);
$options->set('isRemoteEnabled', true);

$dompdf = new Dompdf($options);

$table = '';

foreach ($actions as $key => $actions) {
    $table .= '<table border="1" cellpadding="4" cellspacing="0" style="width: 100%; margin-bottom:0; border-collapse: collapse;">';
    $table .= '<thead>
        <tr style="border: none;">
             <th colspan="2" style="background-color: #f2f2f2; border: 1px solid #000; padding: 8px; text-align: left;">' . (strtoupper($actions['action'])) . '</th>
        </tr>
    </thead>';

    $table .= ' <tbody>';
    $actionId = intval($actions['id']);

    foreach ($actions['attributes'] as $index => $attribute) {
        $table .= '<tr>';
        $aspect = @$aspects->firstMatch([
            'attribute_id' => intval($attribute['id']),
            'action_id' => intval($actionId)
        ]);
        $table .= '<td style="border: 1px solid #000; padding: 8px; width: 60%;">' . (strtoupper($attribute['attribute'])) . '</td>';
        $table .= '<td style="border: 1px solid #000; padding: 8px; width: 40%;">' . (intval(@$aspect['total'])) . '</td>';
        $table .= '</tr>';
    }

    $table .= '</tbody>';
    $table .= '</table>';
}

// HTML content with header and footer
$html = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body{
            padding-right: 2em;
            padding-left: 2em;
            padding-top: 14em;
            padding-bottom: 8em;
        }
    </style>
</head>
<body>
    '.($table).'
     <div style="width: 100%; margin-top: 100px; text-align: left;">
        Prepared By <br>
        <br>
        <br>
     <strong>'.(strtoupper($layout->name)).'</strong> <br>
         '.(strtoupper($layout->position)).'
    </div>
</body>
</html>
';

// Load the HTML content
$dompdf->loadHtml($html);

// Set paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

$canvas = $dompdf->get_canvas();
$canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics) use ($header, $footer){
    $width = $canvas->get_width();
    $height = $canvas->get_height();
    $canvas->image($header, intval(0), intval(15), (intval($width) - intval(10)), intval(150));
    $canvas->image($footer, intval(0), (intval($height) - intval(100)), (intval($width) - intval(10)), intval(80));

    $canvas->text(intval(0), intval(0), '', $fontMetrics->get_font('Arial', 'normal'), 12);
    $canvas->text(intval(50), (intval($height) - intval(20)), "Page $pageNumber of $pageCount", $fontMetrics->get_font('Arial', 'normal'), intval(10));
});

// Output the generated PDF to Browser
$dompdf->stream((ucwords(preg_replace('/\s+/', '_', $filename))).'.pdf', [
    'Attachment' => false
]);
exit(0);
