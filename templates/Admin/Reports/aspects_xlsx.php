<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$count = 0;
$column = 'A';

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$worksheet = $spreadsheet->setActiveSheetIndex(0);

foreach ($actions as $key => $action){

    $count++;

    $actionId = intval($action['id']);

    $worksheet
        ->setCellValue($column.(strval($count)), strtoupper($action['action']))
        ->getStyle($column.(strval($count)))
        ->getFont()
        ->setBold(true);

    $worksheet
        ->mergeCells('A'.(strval($count)).':C'.strval($count))
        ->getStyle($column.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    foreach ($action['attributes'] as $index => $attribute){
        $count++;
        $aspect = @$aspects->firstMatch([
            'attribute_id' => intval($attribute['id']),
            'action_id' => intval($actionId)
        ]);
        $worksheet
            ->setCellValue('B'.(strval($count)), strtoupper($attribute['attribute']))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(false);
        $worksheet
            ->setCellValue('C'.(strval($count)), intval(@$aspect['total']))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(false);
    }

}

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
$writer->save('php://output');
exit(0);