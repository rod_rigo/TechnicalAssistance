<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?=$this->Html->css('admin/reports/elements')?>

<?=$this->Form->create(null,['id' => 'form', 'class' => 'row', 'type' => 'file'])?>
<div class="col-sm-12 col-md-4 col-lg-4 mt-1">
    <?=$this->Form->label('start_date', ucwords('start date'))?>
    <?=$this->Form->date('start_date',[
        'class' => 'form-control',
        'id' => 'start-date',
        'required' => true,
        'title' => ucwords('please fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-4 col-lg-4 mt-1">
    <?=$this->Form->label('end_date', ucwords('end date'))?>
    <?=$this->Form->date('end_date',[
        'class' => 'form-control',
        'id' => 'end-date',
        'required' => true,
        'title' => ucwords('please fill out this field'),
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
    ])?>
</div>
<div class="col-sm-12 col-md-2 col-lg-2 mt-1">
    <?=$this->Form->label('records', ucwords('records'))?>
    <?=$this->Form->number('records',[
        'class' => 'form-control',
        'id' => 'records',
        'required' => true,
        'title' => ucwords('please fill out this field'),
        'min' => 10000,
        'value' => 10000
    ])?>
</div>
<div class="col-sm-12 col-md-2 col-lg-2 mt-1 d-flex justify-content-start align-items-end">
    <?=$this->Form->button('Search',[
        'type' => 'submit',
        'class' => 'btn btn-primary rounded-0'
    ])?>
</div>
<?=$this->Form->end()?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mt-3">
        <button type="button" class="btn btn-success rounded-0" id="excel">
            Export To XLSX
        </button>
        <button type="button" class="btn btn-danger rounded-0" id="pdf">
            Export To PDF
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nature</th>
                        <th>Sub-Type</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/reports/elements')?>

