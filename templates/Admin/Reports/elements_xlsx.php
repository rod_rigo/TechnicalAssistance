<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$count = 0;
$column = 'A';

// Create a new Spreadsheet object
$spreadsheet = new Spreadsheet();

$worksheet = $spreadsheet->setActiveSheetIndex(0);

foreach ($natures as $key => $nature){

    $count++;

    $natureId = intval($nature['id']);

    $worksheet
        ->setCellValue($column.(strval($count)), strtoupper($nature['nature']))
        ->getStyle($column.(strval($count)))
        ->getFont()
        ->setBold(true);

    $worksheet
        ->mergeCells('A'.(strval($count)).':C'.strval($count))
        ->getStyle($column.(strval($count)))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
        ->setWrapText(true);

    foreach ($nature['sub_types'] as $index => $subTypes){
        $count++;
        $element = @$elements->firstMatch([
            'sub_type_id' => intval($subTypes['id']),
            'nature_id' => intval($natureId)
        ]);
        $worksheet
            ->setCellValue('B'.(strval($count)), strtoupper($subTypes['sub_type']))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(false);
        $worksheet
            ->setCellValue('C'.(strval($count)), intval(@$element['total']))
            ->getStyle($column.(strval($count)))
            ->getFont()
            ->setBold(false);
    }

}

$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(120, 'pt');
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(120, 'pt');

$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
$writer->save('php://output');
exit(0);