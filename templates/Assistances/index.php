<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assistance[]|\Cake\Collection\CollectionInterface $assistances
 */
?>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-5">
                <h2 class="heading-section">Technical Assistance Form</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="row no-gutters">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="contact-wrap w-100 p-md-5 p-4">
                                <?=$this->Form->create($entity,['id' => 'form', 'class' => 'contactForm', 'type' => 'file'])?>

                                    <h6 class="mt-3 text-dark">
                                        <strong>Type Of Request</strong>
                                    </h6>
                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group d-flex flex-column justify-content-start align-items-start">
                                                <?php foreach ($accounts as $account):?>
                                                    <div class="icheck-primary m-1">
                                                        <?=$this->Form->checkbox('accounts.'.($account->id),[
                                                            'id' => 'accounts-'.($account->id),
                                                            'hiddenField' => false,
                                                            'class' => 'accounts',
                                                            'value' => $account->id,
                                                            'required' => true,
                                                            //                                                            'checked' => boolval($account->is_default)
                                                        ])?>
                                                        <?=$this->Form->label('accounts.'.($account->id),ucwords($account->account))?>
                                                    </div>
                                                <?php endforeach;?>
                                                <?=$this->Form->hidden('account_id',[
                                                    'id' => 'account-id',
                                                    'required' => true
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <?=$this->Form->label('request_id', ucwords('request'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->select('request_id', [],[
                                                    'id' => 'request-id',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'empty' => ucwords('Choose Account'),
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                    </div>

                                    <h6 class="mt-3 text-dark">
                                        <strong>
                                            Account Type
                                        </strong>
                                    </h6>
                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group d-flex flex-column justify-content-start align-items-start">
                                                <?php foreach ($accountTypes as $key => $value):?>
                                                    <div class="icheck-primary m-1">
                                                        <?=$this->Form->checkbox('account_types.'.($key),[
                                                            'id' => 'account-types-'.($key),
                                                            'hiddenField' => false,
                                                            'class' => 'account-types',
                                                            'value' => $key,
                                                            'required' => true
                                                        ])?>
                                                        <?=$this->Form->label('account_types.'.($key),ucwords($value))?>
                                                    </div>
                                                <?php endforeach;?>
                                                <?=$this->Form->hidden('account_type_id',[
                                                    'id' => 'account-type-id',
                                                    'required' => true
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                    </div>

                                    <h6 class="mt-3 text-dark">
                                        <strong>Nature Of Request</strong>
                                    </h6>
                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <ul class="list-unstyled">
                                                <?php $counter = 0;?>
                                                <?php foreach ($natures as $nature):?>
                                                    <li>
                                                        <strong class="text-primary">
                                                            <?=strtoupper($nature->nature)?>
                                                        </strong>
                                                        <ul style="list-style: none;">
                                                            <?php foreach ($nature->sub_types as $sub_type):?>
                                                                <li>
                                                                    <div class="icheck-primary m-1">
                                                                        <?=$this->Form->checkbox('sub_types[]',[
                                                                            'id' => 'sub-types-'.($counter),
                                                                            'hiddenField' => false,
                                                                            'class' => 'sub-types',
                                                                            'value' => $sub_type->id,
                                                                        ])?>
                                                                        <?=$this->Form->label('sub_types.'.($counter),ucwords($sub_type->sub_type))?>
                                                                    </div>
                                                                </li>
                                                                <?php $counter++?>
                                                            <?php endforeach;?>
                                                        </ul>
                                                    </li>
                                                <?php endforeach;?>
                                            </ul>
                                        </div>
                                    </div>

                                    <h6 class="mt-3 text-dark">
                                        <strong>
                                            Office/School
                                        </strong>
                                    </h6>
                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group d-flex flex-column justify-content-start align-items-start">
                                                <?php foreach ($establishments as $key => $value):?>
                                                    <div class="icheck-primary m-1">
                                                        <?=$this->Form->checkbox('establishments.'.($key),[
                                                            'id' => 'establishments-'.($key),
                                                            'hiddenField' => false,
                                                            'class' => 'establishments',
                                                            'value' => $key,
                                                            'required' => true
                                                        ])?>
                                                        <?=$this->Form->label('establishments.'.($key),ucwords($value))?>
                                                    </div>
                                                <?php endforeach;?>
                                                <?=$this->Form->hidden('establishment_id',[
                                                    'id' => 'establishment-id',
                                                    'required' => true
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <?=$this->Form->label('department_id', ucwords('department'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->select('department_id', [],[
                                                    'id' => 'department-id',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'empty' => ucwords('Choose Establishment'),
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <?=$this->Form->label('description', ucwords('description'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->textarea('description',[
                                                    'id' => 'description',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('Short Description of your Request/Problems Encountered'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                    </div>

                                    <h6 class="mt-3 text-dark">
                                        <strong>Client Information</strong>
                                    </h6>
                                    <div class="row mb-1">
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <?=$this->Form->label('first_name', ucwords('first name'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->text('first_name',[
                                                    'id' => 'first-name',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('first name'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <?=$this->Form->label('middle_name', ucwords('middle name'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->text('middle_name',[
                                                    'id' => 'middle-name',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('middle name'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <?=$this->Form->label('last_name', ucwords('last name'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->text('last_name',[
                                                    'id' => 'last-name',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('last name'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <?=$this->Form->label('deped_email', ucwords('DepEd Email'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->email('deped_email',[
                                                    'id' => 'deped-email',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('DepEd Email'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <?=$this->Form->label('personal_email', ucwords('personal Email'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->email('personal_email',[
                                                    'id' => 'personal-email',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('personal Email'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <?=$this->Form->label('permanent_contact_no', ucwords('permanent contact no'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->text('permanent_contact_no',[
                                                    'id' => 'permanent-contact-no',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'placeholder' => ucwords('permanent contact no'),
                                                    'pattern' => '(09|\+639)([0-9]{9})',
                                                    'title' => ucwords('contact number must be start at 0 & 9 followed by 9 digits')
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <?=$this->Form->label('position', ucwords('position'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->text('position',[
                                                    'id' => 'position',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'placeholder' => ucwords('position'),
                                                    'pattern' => '(.){1,}'
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <?=$this->Form->label('file', ucwords('file'), [
                                                    'class' => 'label'
                                                ])?>
                                                <?=$this->Form->file('file',[
                                                    'id' => 'file',
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'title' => ucwords('please fill out this field'),
                                                    'accept' => 'application/pdf',
                                                ])?>
                                                <small></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-8 col-lg-6">
                                            <embed src="#" id="embed" width="800" loading="lazy" height="0" onwheel>
                                        </div>
                                    </div>

                                    <div class="row d-flex justify-content-end align-items-center">
                                        <div class="form-group">
                                            <?=$this->Form->hidden('file_before',[
                                                'id' => 'file-before',
                                                'value' => uniqid(),
                                                'required' => true
                                            ])?>
                                            <?=$this->Form->hidden('file_after',[
                                                'id' => 'file-after',
                                                'value' => uniqid(),
                                                'required' => true
                                            ])?>
                                            <?=$this->Form->button('Submit',[
                                                'class' => 'btn btn-info',
                                                'type' => 'submit'
                                            ])?>
                                        </div>
                                    </div>
                                <?=$this->Form->end()?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?=$this->Html->script('assistances/index')?>