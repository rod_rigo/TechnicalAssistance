<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/logo.jpg')?>" class="img-thumbnail w-75" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('dashboards') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('assistances') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Assistances
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Assistances', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('assistances') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('reports') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('reports') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-clone"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Reports', 'action' => 'assistances'])?>" class="nav-link <?=(strtolower($controller) == strtolower('reports') && strtolower($action) == strtolower('assistances') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Assistances</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Reports', 'action' => 'elements'])?>" class="nav-link <?=(strtolower($controller) == strtolower('reports') && strtolower($action) == strtolower('elements') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Elements</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Reports', 'action' => 'aspects'])?>" class="nav-link <?=(strtolower($controller) == strtolower('reports') && strtolower($action) == strtolower('aspects') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Aspects</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Configurations</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('users') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Heads', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('heads') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Heads
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Establishments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('establishments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Establishments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Departments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('departments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Departments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Clusters', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('clusters') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-compass"></i>
                        <p>
                            Clusters
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Primaries', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('primaries') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-level-up-alt"></i>
                        <p>
                            Primaries
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('layouts') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-layer-group"></i>
                        <p>
                            Layouts
                        </p>
                    </a>
                </li>

                <li class="nav-header">Components</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Accounts', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('accounts') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            Accounts
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'AccountTypes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('accounttypes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Account Types
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('requests') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fa fa-file"></i>
                        <p>
                            Requests
                        </p>
                    </a>
                </li>

                <li class="nav-header">Nature of Request</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Natures', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('natures') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fa fa-desktop"></i>
                        <p>
                            Natures
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'SubTypes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('subtypes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fa fa-th-list"></i>
                        <p>
                            Sub Types
                        </p>
                    </a>
                </li>

                <li class="nav-header">Action Taken</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Actions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('actions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Actions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Attributes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('attributes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Attributes
                        </p>
                    </a>
                </li>

                <li class="nav-header">Archive</li>
                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="#" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Reports', 'action' => 'assistances'])?>" class="nav-link <?=(strtolower('assistances') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Assistances</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Establishments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('establishments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Establishments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Departments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('departments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Departments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Accounts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('accounts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Accounts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'AccountTypes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('accounttypess') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Account Types</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Clusters', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('clusters') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Clusters</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Actions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('actions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Actions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Attributes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('attributes') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Attributes</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Primaries', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('primaries') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Primaries</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('requests') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Natures', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('natures') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Natures</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'SubTypes', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('subtypes') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sub Types</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Heads', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('heads') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Heads</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Layouts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('layouts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Layouts</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
