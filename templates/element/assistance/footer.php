<?php
/**
 * @var \App\View\AppView $this
 */
?>

<footer class="footer p-0">
    <div class="container">
        <div class="row p-2">
            <div class="col-md-12 text-center">

                <p class="copyright m-0 text-capitalize"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<?=date('Y')?> All rights reserved</p>
            </div>
        </div>
    </div>
</footer>
