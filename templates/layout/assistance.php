<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = ucwords('Technical Assistance');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('/img/logo.jpg','/img/logo.jpg',['type'=>'icon']); ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?= $this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken')) ?>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <?=$this->Html->css([
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
        '/pet-sitting/css/animate',
        '/pet-sitting/css/flaticon',
        '/pet-sitting/css/style'
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/chartjs/js/chart',
        '/ck-editor/js/ckeditor',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2',
        '/turbo-links/js/turbolinks',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/TechnicalAssistance/';
    </script>

</head>
<body>

<div class="wrap">
    <div class="container">
        <div class="row p-4">

        </div>
    </div>
</div>

<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>

<?=$this->element('assistance/footer')?>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<?=$this->Html->script([
    '/pet-sitting/js/jquery.min',
    '/pet-sitting/js/jquery-migrate-3.0.1.min',
    '/pet-sitting/js/popper.min',
    '/pet-sitting/js/bootstrap.min',
    '/pet-sitting/js/jquery.easing.1.3',
    '/pet-sitting/js/jquery.waypoints.min',
    '/pet-sitting/js/jquery.stellar.min',
    '/pet-sitting/js/jquery.animateNumber.min',
    '/pet-sitting/js/scrollax.min',
    '/pet-sitting/js/main'
])?>

</body>
</html>
