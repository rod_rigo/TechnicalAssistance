<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ElementsFixture
 */
class ElementsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'assistance_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'nature_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sub_type_id' => ['type' => 'biginteger', 'length' => null, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'modified' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => ''],
        'deleted' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'elements to assistances' => ['type' => 'index', 'columns' => ['assistance_id'], 'length' => []],
            'elements to natures' => ['type' => 'index', 'columns' => ['nature_id'], 'length' => []],
            'elements to sub_types' => ['type' => 'index', 'columns' => ['sub_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'elements to assistances' => ['type' => 'foreign', 'columns' => ['assistance_id'], 'references' => ['assistances', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'elements to natures' => ['type' => 'foreign', 'columns' => ['nature_id'], 'references' => ['natures', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'elements to sub_types' => ['type' => 'foreign', 'columns' => ['sub_type_id'], 'references' => ['sub_types', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_0900_ai_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'assistance_id' => 1,
                'nature_id' => 1,
                'sub_type_id' => 1,
                'created' => 1710764550,
                'modified' => 1710764550,
                'deleted' => '2024-03-18 20:22:30',
            ],
        ];
        parent::init();
    }
}
