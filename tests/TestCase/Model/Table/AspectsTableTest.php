<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AspectsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AspectsTable Test Case
 */
class AspectsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AspectsTable
     */
    protected $Aspects;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Aspects',
        'app.Assistances',
        'app.Actions',
        'app.SubTypes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Aspects') ? [] : ['className' => AspectsTable::class];
        $this->Aspects = $this->getTableLocator()->get('Aspects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Aspects);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AspectsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\AspectsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
