<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssistancesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AssistancesTable Test Case
 */
class AssistancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AssistancesTable
     */
    protected $Assistances;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Assistances',
        'app.Establishments',
        'app.Departments',
        'app.Accounts',
        'app.Requests',
        'app.Elements',
        'app.Findings',
        'app.Takens',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Assistances') ? [] : ['className' => AssistancesTable::class];
        $this->Assistances = $this->getTableLocator()->get('Assistances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Assistances);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\AssistancesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\AssistancesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
