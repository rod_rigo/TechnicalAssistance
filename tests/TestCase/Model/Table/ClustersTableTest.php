<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClustersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClustersTable Test Case
 */
class ClustersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClustersTable
     */
    protected $Clusters;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Clusters',
        'app.Users',
        'app.Heads',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Clusters') ? [] : ['className' => ClustersTable::class];
        $this->Clusters = $this->getTableLocator()->get('Clusters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Clusters);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ClustersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ClustersTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
