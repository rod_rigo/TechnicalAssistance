<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FindingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FindingsTable Test Case
 */
class FindingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FindingsTable
     */
    protected $Findings;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Findings',
        'app.Assistances',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Findings') ? [] : ['className' => FindingsTable::class];
        $this->Findings = $this->getTableLocator()->get('Findings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Findings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FindingsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FindingsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
