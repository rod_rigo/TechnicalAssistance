<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HeadsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HeadsTable Test Case
 */
class HeadsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HeadsTable
     */
    protected $Heads;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Heads',
        'app.Users',
        'app.Primaries',
        'app.Clusters',
        'app.Schools',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Heads') ? [] : ['className' => HeadsTable::class];
        $this->Heads = $this->getTableLocator()->get('Heads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Heads);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\HeadsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\HeadsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
