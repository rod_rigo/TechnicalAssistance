<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrimariesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrimariesTable Test Case
 */
class PrimariesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PrimariesTable
     */
    protected $Primaries;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Primaries',
        'app.Users',
        'app.Heads',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Primaries') ? [] : ['className' => PrimariesTable::class];
        $this->Primaries = $this->getTableLocator()->get('Primaries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Primaries);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PrimariesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PrimariesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
