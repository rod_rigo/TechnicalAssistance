<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubTypesTable Test Case
 */
class SubTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubTypesTable
     */
    protected $SubTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SubTypes',
        'app.Users',
        'app.Natures',
        'app.Elements',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SubTypes') ? [] : ['className' => SubTypesTable::class];
        $this->SubTypes = $this->getTableLocator()->get('SubTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SubTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test query method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::query()
     */
    public function testQuery(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteAll method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::deleteAll()
     */
    public function testDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getSoftDeleteField method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::getSoftDeleteField()
     */
    public function testGetSoftDeleteField(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDelete method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::hardDelete()
     */
    public function testHardDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test hardDeleteAll method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::hardDeleteAll()
     */
    public function testHardDeleteAll(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test restore method
     *
     * @return void
     * @uses \App\Model\Table\SubTypesTable::restore()
     */
    public function testRestore(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
