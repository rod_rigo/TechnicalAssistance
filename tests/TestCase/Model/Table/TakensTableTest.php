<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TakensTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TakensTable Test Case
 */
class TakensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TakensTable
     */
    protected $Takens;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Takens',
        'app.Assistances',
        'app.Actions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Takens') ? [] : ['className' => TakensTable::class];
        $this->Takens = $this->getTableLocator()->get('Takens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Takens);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\TakensTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\TakensTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
