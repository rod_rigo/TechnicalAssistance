'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'assistances/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];

    var title = function (e) {
        var startDate = moment().startOf('year').format('Y-MM-DD');
        var endDate = moment().endOf('year').format('Y-MM-DD');
        return 'Reports Of '+(startDate)+' - '+(endDate);
    };

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getAssistancesYear',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];

                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableBodyEven.fontSize = 8;
                    doc.styles.tableBodyOdd.fontSize = 8;
                    doc.styles.tableFooter.fontSize = 8;

                    // Adjust column widths if needed
                    var columns = doc.content[1].table.body[0].length;
                    var colWidths = [];
                    if (columns <= 19) {
                        // Calculate equal width for up to 20 columns
                        colWidths = Array(columns + 1).fill('*');
                    } else {
                        // Set specific width for each column if there are more than 20 columns
                        // Adjust the values as needed
                        colWidths = Array(21).fill('*');
                    }
                    doc.content[1].table.widths = colWidths;
                    doc.defaultStyle.alignment = 'center';
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [1],
                data: null,
                render: function(data,type,row,meta){
                    return row.first_name +' '+ row.middle_name +' '+ row.last_name;
                }
            },
            {
                targets: [13],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [14],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white pdf" title="PDF">PDF</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'first_name'},
            { data: 'position'},
            { data: 'establishment.establishment'},
            { data: 'department.department'},
            { data: 'account_type.account_type'},
            { data: 'account.account'},
            { data: 'request.request'},
            { data: 'deped_email'},
            { data: 'personal_email'},
            { data: 'permanent_contact_no'},
            { data: 'description'},
            { data: 'other_details'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(parseInt(dataId));
        Swal.fire({
            icon: 'question',
            title: null,
            text: 'Open In New Window?',
            showDenyButton: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
            confirmButtonColor: '#3085d6',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
            denyButtonColor: '#6e7881',
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            } else if (result.isDenied) {
                Swal.close();
            }else{
                Turbolinks.visit(href,{action:'replace'});
            }
        });
    });

    datatable.on('click','.pdf',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'pdf/'+(parseInt(dataId));
        Swal.fire({
            icon: 'question',
            title: null,
            text: 'Open In New Window?',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes',
            confirmButtonColor: '#3085d6',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
            Swal.close();
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});