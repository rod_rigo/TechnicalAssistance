'use strict';
$(document).ready(function () {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#year').change(function (e) {
        var value = $(this).val();
        if(value){
            assistance_chart_month.destroy();
            getAssistancesChart();

            assistance_department_chart.destroy();
            getAssistancesDepartmentsChart();

            assistance_account_chart.destroy();
            getAssistancesAccountsChart();

            assistance_request_chart.destroy();
            getAssistancesRequestsChart();

            assistance_sub_type_chart.destroy();
            getAssistancesSubTypesChart();

            assistance_attribute_chart.destroy();
            getAssistancesAttributesChart();
        }
    });

    $('#account').change(function (e) {
        var value = $(this).val();
        if(value){
            assistance_request_chart.destroy();
            getAssistancesRequestsChart();
        }
    });

    $('#nature').change(function (e) {
        var value = $(this).val();
        if(value){
            assistance_sub_type_chart.destroy();
            getAssistancesSubTypesChart();
        }
    });

    $('#action').change(function (e) {
        var value = $(this).val();
        if(value){
            assistance_attribute_chart.destroy();
            getAssistancesAttributesChart();
        }
    });

    var assistance_canvas_month = document.querySelector('#assistance-chart-month').getContext('2d');
    var assistance_chart_month;

    function getAssistances() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistances?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesChart() {
        assistance_chart_month =  new Chart(assistance_canvas_month, {
            type: 'bar',
            data: {
                labels: getAssistances().label,
                datasets: [
                    {
                        label:'Monthly Assistances',
                        data: getAssistances().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: true,
                        text: 'Monthly Assistance',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var assistance_department_canvas = document.querySelector('#assistance-department-chart').getContext('2d');
    var assistance_department_chart;

    function getAssistancesDepartments() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistancesDepartments?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.department).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesDepartmentsChart() {
        assistance_department_chart =  new Chart(assistance_department_canvas, {
            type: 'doughnut',
            data: {
                labels: getAssistancesDepartments().label,
                datasets: [
                    {
                        label:'Assistance',
                        data: getAssistancesDepartments().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 2
                    },
                    title: {
                        display: true,
                        text: 'Assistance By Department',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var assistance_account_canvas = document.querySelector('#assistance-account-chart').getContext('2d');
    var assistance_account_chart;

    function getAssistancesAccounts() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistancesAccounts?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.account).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesAccountsChart() {
        assistance_account_chart =  new Chart(assistance_account_canvas, {
            type: 'pie',
            data: {
                labels: getAssistancesAccounts().label,
                datasets: [
                    {
                        label:'Assistance',
                        data: getAssistancesAccounts().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 3
                    },
                    title: {
                        display: true,
                        text: 'Assistance By Account',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var assistance_request_canvas = document.querySelector('#assistance-request-chart').getContext('2d');
    var assistance_request_chart;

    function getAssistancesRequests() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistancesRequests/'+($('#account').val())+'?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.request).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesRequestsChart() {
        assistance_request_chart =  new Chart(assistance_request_canvas, {
            type: 'polarArea',
            data: {
                labels: getAssistancesRequests().label,
                datasets: [
                    {
                        label:'Requests',
                        data: getAssistancesRequests().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 4
                    },
                    title: {
                        display: true,
                        text: 'Account By Requests',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var assistance_sub_type_canvas = document.querySelector('#assistance-sub-type-chart').getContext('2d');
    var assistance_sub_type_chart;

    function getAssistancesSubTypes() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistancesSubTypes/'+($('#nature').val())+'?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.sub_type).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesSubTypesChart() {
        assistance_sub_type_chart =  new Chart(assistance_sub_type_canvas, {
            type: 'line',
            data: {
                labels: getAssistancesSubTypes().label,
                datasets: [
                    {
                        label:'Sub Types',
                        data: getAssistancesSubTypes().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Nature By Sub-Types',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var assistance_attribute_canvas = document.querySelector('#assistance-attribute-chart').getContext('2d');
    var assistance_attribute_chart;

    function getAssistancesAttributes() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAssistancesAttributes/'+($('#action').val())+'?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.attribute).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getAssistancesAttributesChart() {
        assistance_attribute_chart =  new Chart(assistance_attribute_canvas, {
            type: 'bar',
            data: {
                labels: getAssistancesAttributes().label,
                datasets: [
                    {
                        label:'Attributes',
                        data: getAssistancesAttributes().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 6
                    },
                    title: {
                        display: true,
                        text: 'Action By Attributes',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function assistances() {
        $.ajax({
            url:baseurl+'assistances',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#total-assistances-today, #total-assistances-week, #total-assistances-month, #total-assistances-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-assistances-today').text(parseFloat(data.today));
            $('#total-assistances-week').text(parseFloat(data.week));
            $('#total-assistances-month').text(parseFloat(data.month));
            $('#total-assistances-year').text(parseFloat(data.year));
        }).fail(function (data, status, xhr) {

        });
    }

    function counts() {
        $.ajax({
            url:baseurl+'counts',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#departments, #accounts, #requests').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#departments').text(parseFloat(data.departments));
            $('#accounts').text(parseFloat(data.accounts));
            $('#requests').text(parseFloat(data.requests));
        }).fail(function (data, status, xhr) {

        });
    }

    counts();
    assistances();
    getAssistancesChart();
    getAssistancesDepartmentsChart();
    getAssistancesAccountsChart();
    getAssistancesRequestsChart();
    getAssistancesSubTypesChart();
    getAssistancesAttributesChart();

});