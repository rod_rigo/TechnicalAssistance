'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#name').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#positiion').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});
$(document).ready(function (e) {

    var cropper;
    var zoom = 0;
    var scaleY = false;
    var scaleX = false;

    cropper = new Cropper( document.querySelector('#header-cropper'), {
        dragMode: 'move',
        aspectRatio: 'free',
        initialAspectRatio: 'free',
        autoCrop:true,
        responsive:true,
        restore:true,
        checkOrientation:true,
        modal:true,
        guides:true,
        center:true,
        highlight:true,
        background:true,
        autoCropArea: 0.8,
        movable:true,
        rotatable:true,
        scalable:true,
        zoomable:true,
        zoomOnTouch:true,
        zoomOnWheel:true,
        wheelZoomRatio:true,
        cropBoxMovable:true,
        cropBoxResizable:true,
        toggleDragModeOnDblclick:true,
        minContainerWidth:200,
        minContainerHeight:200,
        minCanvasWidth:200,
        minCanvasHeight:200,
        minCropBoxWidth:50,
        minCropBoxHeight:50,
    });

    $('#header-crop').click(function () {
        cropper.setDragMode('crop');
    });

    $('#header-move').click(function () {
        cropper.setDragMode('move');
    });

    $('#header-zoom-in').click(function (e) {
        cropper.zoom(parseFloat(0.5));
    });

    $('#header-zoom-out').click(function (e) {
        cropper.zoom(-parseFloat(0.5));
    });

    $('#header-rotate-left').click(function (e) {
        cropper.rotate(-parseFloat(90));
    });

    $('#header-rotate-right').click(function (e) {
        cropper.rotate(parseFloat(90));
    });

    $('#header-flip-vertical').click(function (e) {
        if(scaleY){
            cropper.scaleY(1);
            scaleY = false;
        }else{
            cropper.scaleY(-1);
            scaleY = true;
        }
    });

    $('#header-flip-horizontal').click(function (e) {
        if(scaleX){
            cropper.scaleX(1);
            scaleX = false;
        }else{
            cropper.scaleX(-1);
            scaleX = true;
        }
    });

    $('#header-move-up').click(function (e) {
        cropper.move(parseInt(0), -parseInt(10));
    });

    $('#header-move-down').click(function (e) {
        cropper.move(parseInt(0), parseInt(10));
    });

    $('#header-move-left').click(function (e) {
        cropper.move(-parseInt(10), parseInt(0));
    });

    $('#header-move-right').click(function (e) {
        cropper.move(parseInt(10), parseInt(0));
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#image-preview').attr('src', '#');
    });

    $('#header-view').click(function (e) {
        var src = cropper.getCroppedCanvas({
            width: parseInt($('#header-width').val()),
            height: parseInt($('#header-height').val()),
            fillColor:'#fff',
            imageSmoothingEnabled: false,
            imageSmoothingQuality: 'high',
        }).toDataURL('image/jpeg');
        $('#image-preview').attr('src',src);
        $('#header').val(src);
        $('#modal').modal('toggle');
    });

    $('#header-file').change(function (e) {
        var file = e.target.files[0];
        var mimes = ['image/png', 'image/jpeg', 'image/jpg', 'image/jfif', 'image/webp'];
        var reader = new FileReader();

        if(!mimes.includes(file.type)){
            $(this).addClass('is-invalid').parent().parent().next('small').text('Invalid Image Format');
            return true;
        }

        reader.addEventListener('load', function (e) {
            if(cropper){
                cropper.destroy();
            }
            $('#header-cropper').attr('src', e.target.result);
            cropper = new Cropper( document.querySelector('#header-cropper'), {
                dragMode: 'move',
                aspectRatio: 'free',
                initialAspectRatio: 'free',
                autoCrop:true,
                responsive:true,
                restore:true,
                checkOrientation:true,
                modal:true,
                guides:true,
                center:true,
                highlight:true,
                background:true,
                autoCropArea: 0.8,
                movable:true,
                rotatable:true,
                scalable:true,
                zoomable:true,
                zoomOnTouch:true,
                zoomOnWheel:true,
                wheelZoomRatio:true,
                cropBoxMovable:true,
                cropBoxResizable:true,
                toggleDragModeOnDblclick:true,
                minContainerWidth:200,
                minContainerHeight:200,
                minCanvasWidth:200,
                minCanvasHeight:200,
                minCropBoxWidth:50,
                minCropBoxHeight:50,
            });
        });

        $(this).removeClass('is-invalid').parent().parent().next('small').empty();
        $(this).next('label.custom-file-label').text(file.name);
        reader.readAsDataURL(file);
    });

    $('#header-height').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#header-width').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $(window).resize(function (e) {
        if(cropper){
            cropper.destroy();
        }
        cropper = new Cropper( document.querySelector('#header-cropper'), {
            dragMode: 'move',
            aspectRatio: 'free',
            initialAspectRatio: 'free',
            autoCrop:true,
            responsive:true,
            restore:true,
            checkOrientation:true,
            modal:true,
            guides:true,
            center:true,
            highlight:true,
            background:true,
            autoCropArea: 0.8,
            movable:true,
            rotatable:true,
            scalable:true,
            zoomable:true,
            zoomOnTouch:true,
            zoomOnWheel:true,
            wheelZoomRatio:true,
            cropBoxMovable:true,
            cropBoxResizable:true,
            toggleDragModeOnDblclick:true,
            minContainerWidth:200,
            minContainerHeight:200,
            minCanvasWidth:200,
            minCanvasHeight:200,
            minCropBoxWidth:50,
            minCropBoxHeight:50,
        });
    });

});
$(document).ready(function (e) {

    var cropper;
    var zoom = 0;
    var scaleY = false;
    var scaleX = false;

    cropper = new Cropper( document.querySelector('#footer-cropper'), {
        dragMode: 'move',
        aspectRatio: 'free',
        initialAspectRatio: 'free',
        autoCrop:true,
        responsive:true,
        restore:true,
        checkOrientation:true,
        modal:true,
        guides:true,
        center:true,
        highlight:true,
        background:true,
        autoCropArea: 0.8,
        movable:true,
        rotatable:true,
        scalable:true,
        zoomable:true,
        zoomOnTouch:true,
        zoomOnWheel:true,
        wheelZoomRatio:true,
        cropBoxMovable:true,
        cropBoxResizable:true,
        toggleDragModeOnDblclick:true,
        minContainerWidth:200,
        minContainerHeight:200,
        minCanvasWidth:200,
        minCanvasHeight:200,
        minCropBoxWidth:50,
        minCropBoxHeight:50,
    });

    $('#footer-crop').click(function () {
        cropper.setDragMode('crop');
    });

    $('#footer-move').click(function () {
        cropper.setDragMode('move');
    });

    $('#footer-zoom-in').click(function (e) {
        cropper.zoom(parseFloat(0.5));
    });

    $('#footer-zoom-out').click(function (e) {
        cropper.zoom(-parseFloat(0.5));
    });

    $('#footer-rotate-left').click(function (e) {
        cropper.rotate(-parseFloat(90));
    });

    $('#footer-rotate-right').click(function (e) {
        cropper.rotate(parseFloat(90));
    });

    $('#footer-flip-vertical').click(function (e) {
        if(scaleY){
            cropper.scaleY(1);
            scaleY = false;
        }else{
            cropper.scaleY(-1);
            scaleY = true;
        }
    });

    $('#footer-flip-horizontal').click(function (e) {
        if(scaleX){
            cropper.scaleX(1);
            scaleX = false;
        }else{
            cropper.scaleX(-1);
            scaleX = true;
        }
    });

    $('#footer-move-up').click(function (e) {
        cropper.move(parseInt(0), -parseInt(10));
    });

    $('#footer-move-down').click(function (e) {
        cropper.move(parseInt(0), parseInt(10));
    });

    $('#footer-move-left').click(function (e) {
        cropper.move(-parseInt(10), parseInt(0));
    });

    $('#footer-move-right').click(function (e) {
        cropper.move(parseInt(10), parseInt(0));
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#image-preview').attr('src', '#');
    });

    $('#footer-view').click(function (e) {
        var src = cropper.getCroppedCanvas({
            width: parseInt($('#width').val()),
            height: parseInt($('#height').val()),
            fillColor:'#fff',
            imageSmoothingEnabled: false,
            imageSmoothingQuality: 'high',
        }).toDataURL('image/jpeg');
        $('#image-preview').attr('src',src);
        $('#footer').val(src);
        $('#modal').modal('toggle');
    });

    $('#footer-file').change(function (e) {
        var file = e.target.files[0];
        var mimes = ['image/png', 'image/jpeg', 'image/jpg', 'image/jfif', 'image/webp'];
        var reader = new FileReader();

        if(!mimes.includes(file.type)){
            $(this).addClass('is-invalid').parent().parent().next('small').text('Invalid Image Format');
            return true;
        }

        reader.addEventListener('load', function (e) {
            if(cropper){
                cropper.destroy();
            }
            $('#footer-cropper').attr('src', e.target.result);
            cropper = new Cropper( document.querySelector('#footer-cropper'), {
                dragMode: 'move',
                aspectRatio: 'free',
                initialAspectRatio: 'free',
                autoCrop:true,
                responsive:true,
                restore:true,
                checkOrientation:true,
                modal:true,
                guides:true,
                center:true,
                highlight:true,
                background:true,
                autoCropArea: 0.8,
                movable:true,
                rotatable:true,
                scalable:true,
                zoomable:true,
                zoomOnTouch:true,
                zoomOnWheel:true,
                wheelZoomRatio:true,
                cropBoxMovable:true,
                cropBoxResizable:true,
                toggleDragModeOnDblclick:true,
                minContainerWidth:200,
                minContainerHeight:200,
                minCanvasWidth:200,
                minCanvasHeight:200,
                minCropBoxWidth:50,
                minCropBoxHeight:50,
            });
        });

        $(this).removeClass('is-invalid').parent().parent().next('small').empty();
        $(this).next('label.custom-file-label').text(file.name);
        reader.readAsDataURL(file);
    });

    $('#footer-height').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#footer-width').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $(window).resize(function (e) {
        if(cropper){
            cropper.destroy();
        }
        cropper = new Cropper( document.querySelector('#footer-cropper'), {
            dragMode: 'move',
            aspectRatio: 'free',
            initialAspectRatio: 'free',
            autoCrop:true,
            responsive:true,
            restore:true,
            checkOrientation:true,
            modal:true,
            guides:true,
            center:true,
            highlight:true,
            background:true,
            autoCropArea: 0.8,
            movable:true,
            rotatable:true,
            scalable:true,
            zoomable:true,
            zoomOnTouch:true,
            zoomOnWheel:true,
            wheelZoomRatio:true,
            cropBoxMovable:true,
            cropBoxResizable:true,
            toggleDragModeOnDblclick:true,
            minContainerWidth:200,
            minContainerHeight:200,
            minCanvasWidth:200,
            minCanvasHeight:200,
            minCropBoxWidth:50,
            minCropBoxHeight:50,
        });
    });


});