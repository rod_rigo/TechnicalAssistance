'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'reports/';
    var url = '';

    var title = function (e) {
        var startDate = ($('#start-date').val())? moment($('#start-date').val()).format('Y-MM-DD'): moment().startOf('year').format('Y-MM-DD');
        var endDate = ($('#end-date').val())? moment($('#end-date').val()).format('Y-MM-DD'): moment().endOf('year').format('Y-MM-DD');
        return 'Reports Of '+(startDate)+' - '+(endDate);
    };

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getElements',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];

                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableBodyEven.fontSize = 8;
                    doc.styles.tableBodyOdd.fontSize = 8;
                    doc.styles.tableFooter.fontSize = 8;

                    // Adjust column widths if needed
                    var columns = doc.content[1].table.body[0].length;
                    var colWidths = [];
                    if (columns <= 19) {
                        // Calculate equal width for up to 20 columns
                        colWidths = Array(columns + 1).fill('*');
                    } else {
                        // Set specific width for each column if there are more than 20 columns
                        // Adjust the values as needed
                        colWidths = Array(21).fill('*');
                    }
                    doc.content[1].table.widths = colWidths;
                    doc.defaultStyle.alignment = 'center';
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.total)));
                }
            },
        ],
        columns: [
            { data: 'total'},
            { data: 'nature'},
            { data: 'sub_type'},
            { data: 'total'},
        ]
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        $('button[type="submit"]').prop('disabled', true);
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = parseInt($('#records').val());
        url = baseurl+'getElements?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records);
        table.ajax.url(url).load(function () {
            $('button[type="submit"]').prop('disabled', false);
        }, false);
    });

    $('#excel').click(function (e) {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = parseInt($('#records').val());
        url = baseurl+'elementsXlsx?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records);
        window.open(url);
    });

    $('#pdf').click(function (e) {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = parseInt($('#records').val());
        url = baseurl+'elementsPdf?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records);
        window.open(url);
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});