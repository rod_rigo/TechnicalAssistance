'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'assistances/add';

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#first-name, #middle-name, #last-name, #position, #description').on('input', function (e) {
        var value = $(this).val();
        var regex = /^(.){1,}$/;

        if(!value.match(regex)){
            $(this).addClass('border-danger').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('border-danger').next('small').empty();
    });

    $('#personal-email').on('input', function (e) {
        var value = $(this).val();
        var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(!value.match(regex)){
            $(this).addClass('border-danger').next('small').text('Please Enter Valid Email');
            return true;
        }

        $(this).removeClass('border-danger').next('small').empty();
    });

    $('#permanent-contact-no').on('input', function (e) {
        var regex = /^(09|\+639)([0-9]{9})$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('border-danger').next('small').text('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
            return true;
        }

        $(this).removeClass('border-danger').next('small').empty();

    });

    $('.account-types').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true).prop('required', true);
        $('.account-types').not(this).prop('checked', false).removeAttr('required');
        $('#account-type-id').val(value);
    });

    $('.establishments').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true).prop('required', true);
        $('.establishments').not(this).prop('checked', false).removeAttr('required');
        $('#establishment-id').val(value);
        getDepartmentsList(value);
    });

    $('#department-id').change(function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('border-danger').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('border-danger').next('small').empty();
    });

    $('.accounts').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true).prop('required', true);
        $('.accounts').not(this).prop('checked', false).removeAttr('required');
        $('#account-id').val(value);
        getRequestsList(value);
    });

    $('#request-id').change(function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('border-danger').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('border-danger').next('small').empty();
    });

    $('#file').change(function (e) {
        var file = e.target.files[0];
        var mimes = [
            'application/pdf'
        ];
        var reader = new FileReader();

        if(!mimes.includes(file.type)){
            $(this).addClass('border-danger').next('small').text('Only PDF File Is Allowed');
            return true;
        }

        reader.addEventListener('load', function (e) {
            $('#embed').attr('src', e.target.result).attr('height', 800)
        });

        $(this).removeClass('border-danger').next('small').empty();
        reader.readAsDataURL(file);
    });

    function getDepartmentsList(establishmentId) {
        $.ajax({
            url: mainurl+'departments/getDepartmentsList/'+(establishmentId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#department-id').prop('disabled', true).html('<option value="">Please Wait...</option>');
            },
        }).done(function (data, status, xhr) {
            $('#department-id').empty().append('<option value="">Select Department</option>');
            $.map(data, function (data, key) {
                $('#department-id').append('<option value="'+(key)+'">'+(data)+'</option>');
            });
            $('#department-id').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            $('#department-id').prop('disabled', false);
            Swal.close();
        });
    }

    function getRequestsList(accountId) {
        $.ajax({
            url: mainurl+'requests/getRequestsList/'+(accountId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#request-id').prop('disabled', true).html('<option value="">Please Wait...</option>');
            },
        }).done(function (data, status, xhr) {
            $('#request-id').empty().append('<option value="">Select Request</option>');
            $.map(data, function (data, key) {
                $('#request-id').append('<option value="'+(key)+'">'+(data)+'</option>');
            });
            $('#request-id').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            $('#request-id').prop('disabled', false);
            Swal.close();
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        });
    }

});