/*
 Navicat Premium Data Transfer

 Source Server         : wampp
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : technical_assistance_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 10/04/2024 15:14:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `is_default` tinyint NOT NULL DEFAULT 0,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `accounts to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `accounts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES (7, 1, 'None', 1, 1, 7, '2024-03-11 17:06:58', '2024-03-11 17:28:46', NULL);
INSERT INTO `accounts` VALUES (8, 1, 'DepEd', 1, 0, 1, '2024-03-11 17:26:57', '2024-03-12 00:00:05', NULL);
INSERT INTO `accounts` VALUES (9, 1, 'O365', 1, 0, 2, '2024-03-11 17:27:14', '2024-03-20 03:13:37', NULL);
INSERT INTO `accounts` VALUES (10, 1, 'DPDS', 1, 0, 3, '2024-03-11 17:27:25', '2024-03-11 17:27:25', NULL);
INSERT INTO `accounts` VALUES (11, 1, 'WINS', 1, 0, 4, '2024-03-11 17:27:38', '2024-03-11 17:27:38', NULL);

-- ----------------------------
-- Table structure for actions
-- ----------------------------
DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `is_others` tinyint NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `actions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `actions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of actions
-- ----------------------------
INSERT INTO `actions` VALUES (2, 1, 'DEPED EMAIL PROCESSED', 1, 0, 1, '2024-03-10 22:23:08', '2024-03-24 23:39:27', NULL);
INSERT INTO `actions` VALUES (3, 1, 'HARDWARE PROBLEM RESOLVED', 1, 0, 3, '2024-03-18 22:08:19', '2024-03-24 23:54:39', NULL);
INSERT INTO `actions` VALUES (4, 1, 'HARDWARE PROBLEM FORWARDED TO', 1, 0, 5, '2024-03-18 22:08:27', '2024-03-24 23:55:02', NULL);
INSERT INTO `actions` VALUES (5, 1, 'SOFTWARE PROBLEM RESOLVED', 1, 0, 2, '2024-03-20 03:09:16', '2024-03-20 03:09:16', NULL);
INSERT INTO `actions` VALUES (6, 1, 'SOFTWARE PROBLEM PENDING', 1, 0, 4, '2024-03-20 03:09:32', '2024-03-20 03:09:32', NULL);

-- ----------------------------
-- Table structure for aspects
-- ----------------------------
DROP TABLE IF EXISTS `aspects`;
CREATE TABLE `aspects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `assistance_id` bigint UNSIGNED NOT NULL,
  `action_id` bigint UNSIGNED NOT NULL,
  `attribute_id` bigint UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `aspects to assistances`(`assistance_id` ASC) USING BTREE,
  INDEX `aspects to actions`(`action_id` ASC) USING BTREE,
  INDEX `aspects to attributes`(`attribute_id` ASC) USING BTREE,
  CONSTRAINT `aspects to actions` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `aspects to assistances` FOREIGN KEY (`assistance_id`) REFERENCES `assistances` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `aspects to attributes` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aspects
-- ----------------------------
INSERT INTO `aspects` VALUES (11, 1, 3, 4, '2024-03-26 22:52:12', '2024-03-26 22:52:12', NULL);

-- ----------------------------
-- Table structure for assistances
-- ----------------------------
DROP TABLE IF EXISTS `assistances`;
CREATE TABLE `assistances`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `account_id` bigint UNSIGNED NOT NULL,
  `request_id` bigint UNSIGNED NOT NULL,
  `deped_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `personal_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `permanent_contact_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `other_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `file_before` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `file_after` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assistances to establishments`(`establishment_id` ASC) USING BTREE,
  INDEX `assistances to departments`(`department_id` ASC) USING BTREE,
  INDEX `assistances to accounts`(`account_id` ASC) USING BTREE,
  INDEX `assistances to requests`(`request_id` ASC) USING BTREE,
  CONSTRAINT `assistances to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assistances to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assistances to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assistances to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assistances
-- ----------------------------
INSERT INTO `assistances` VALUES (1, 'MARC JEFFERSON', 'GERVACIO', 'CALAUNAN', 'Administrative Officer IV', 2, 19, 8, 1, 'joppycalaunan@deped.gov.ph', 'joppycalaunan@gmail.com', '09173191479', '-', '', '', '', '2024-03-20 04:56:20', '2024-03-26 22:52:12', NULL);
INSERT INTO `assistances` VALUES (2, 'JOHN', 'S', 'DOE', 'Administratve Officer 5', 1, 43, 8, 1, 'cabotaje.rodrigo@gmail.com', 'cabotaje.rodrigo@gmail.com', '09100575676', 'Aaaaa', NULL, 'assistance-file/66163a55b4bd5dummy.pdf', 'assistance-file/66163b5332ff7dummy.pdf', '2024-04-10 22:16:24', '2024-04-10 23:10:11', NULL);

-- ----------------------------
-- Table structure for attributes
-- ----------------------------
DROP TABLE IF EXISTS `attributes`;
CREATE TABLE `attributes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `action_id` bigint UNSIGNED NOT NULL,
  `attribute` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT 1,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `attributes to users`(`user_id` ASC) USING BTREE,
  INDEX `attributes to actions`(`action_id` ASC) USING BTREE,
  CONSTRAINT `attributes to actions` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `attributes to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of attributes
-- ----------------------------
INSERT INTO `attributes` VALUES (1, 1, 2, 'CREATED', 3, 1, '2024-03-24 23:12:40', '2024-03-24 23:12:56', NULL);
INSERT INTO `attributes` VALUES (2, 1, 2, 'RESET', 2, 1, '2024-03-24 23:13:02', '2024-03-24 23:13:02', NULL);
INSERT INTO `attributes` VALUES (3, 1, 2, 'TRANSFERRED OUT', 1, 1, '2024-03-24 23:13:13', '2024-03-24 23:13:20', NULL);
INSERT INTO `attributes` VALUES (4, 1, 3, 'GOOD', 1, 1, '2024-03-24 23:13:34', '2024-03-24 23:13:40', NULL);
INSERT INTO `attributes` VALUES (5, 1, 3, 'RETURNED', 2, 1, '2024-03-24 23:13:57', '2024-03-24 23:13:57', NULL);
INSERT INTO `attributes` VALUES (6, 1, 4, 'SERVICE CENTER', 1, 1, '2024-03-24 23:14:07', '2024-03-24 23:14:07', NULL);
INSERT INTO `attributes` VALUES (7, 1, 4, 'FOR PART REPLACEMENT & PROCUREMENT', 2, 1, '2024-03-24 23:14:30', '2024-03-24 23:17:09', NULL);
INSERT INTO `attributes` VALUES (8, 1, 4, 'UNSERVICEABLE', 3, 1, '2024-03-24 23:14:37', '2024-03-24 23:17:07', NULL);

-- ----------------------------
-- Table structure for clusters
-- ----------------------------
DROP TABLE IF EXISTS `clusters`;
CREATE TABLE `clusters`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `cluster` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `clusters to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `clusters to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of clusters
-- ----------------------------
INSERT INTO `clusters` VALUES (1, 1, 'NORTH', 1, '2024-03-10 22:49:03', '2024-03-10 22:51:50', NULL);
INSERT INTO `clusters` VALUES (2, 1, 'EAST', 1, '2024-03-11 17:07:23', '2024-03-11 17:07:23', NULL);
INSERT INTO `clusters` VALUES (3, 1, 'SOUTH', 1, '2024-03-11 17:07:29', '2024-03-11 17:07:29', NULL);
INSERT INTO `clusters` VALUES (4, 1, 'WEST', 1, '2024-03-11 17:07:34', '2024-03-11 17:07:34', NULL);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `is_for_school` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `departments to users`(`user_id` ASC) USING BTREE,
  INDEX `departments to establishments`(`establishment_id` ASC) USING BTREE,
  CONSTRAINT `departments to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `departments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (2, 1, 2, 'Batal Elementary School', 1, 1, '2024-03-20 03:26:30', '2024-03-20 03:26:30', NULL);
INSERT INTO `departments` VALUES (3, 1, 2, 'Divisoria Elementary School', 1, 1, '2024-03-20 03:26:46', '2024-03-20 03:26:46', NULL);
INSERT INTO `departments` VALUES (4, 1, 2, 'Mabini Elementary School', 1, 1, '2024-03-20 03:27:09', '2024-03-20 03:27:09', NULL);
INSERT INTO `departments` VALUES (5, 1, 2, 'Naggasican Elementary School', 1, 1, '2024-03-20 03:27:27', '2024-03-20 03:36:40', NULL);
INSERT INTO `departments` VALUES (6, 1, 2, 'Salvador Integrated School', 1, 1, '2024-03-20 03:27:41', '2024-03-20 03:27:41', NULL);
INSERT INTO `departments` VALUES (7, 1, 2, 'San Andres Elementary School', 1, 1, '2024-03-20 03:28:12', '2024-03-20 03:28:12', NULL);
INSERT INTO `departments` VALUES (8, 1, 2, 'Santiago East Central School', 1, 1, '2024-03-20 03:28:31', '2024-03-20 03:28:31', NULL);
INSERT INTO `departments` VALUES (9, 1, 2, 'Abra Elementary School', 1, 1, '2024-03-20 03:28:43', '2024-03-20 03:28:43', NULL);
INSERT INTO `departments` VALUES (10, 1, 2, 'Ambalatungan Elementary School', 1, 1, '2024-03-20 03:28:58', '2024-03-20 03:28:58', NULL);
INSERT INTO `departments` VALUES (11, 1, 2, 'Baptista Village Elementary School', 1, 1, '2024-03-20 03:29:16', '2024-03-20 03:29:16', NULL);
INSERT INTO `departments` VALUES (12, 1, 2, 'Buenavista Elementary School', 1, 1, '2024-03-20 03:29:35', '2024-03-20 03:29:35', NULL);
INSERT INTO `departments` VALUES (13, 1, 2, 'Cabulay Elementary School', 1, 1, '2024-03-20 03:29:55', '2024-03-20 03:29:55', NULL);
INSERT INTO `departments` VALUES (14, 1, 2, 'Malini Elementary School', 1, 1, '2024-03-20 03:30:08', '2024-03-20 03:30:08', NULL);
INSERT INTO `departments` VALUES (15, 1, 2, 'Nabbuan Integrated School', 1, 1, '2024-03-20 03:30:37', '2024-03-20 03:30:37', NULL);
INSERT INTO `departments` VALUES (16, 1, 2, 'Sagana Elementary School', 1, 1, '2024-03-20 03:30:52', '2024-03-20 03:30:52', NULL);
INSERT INTO `departments` VALUES (17, 1, 2, 'Santiago North Central School', 1, 1, '2024-03-20 03:31:10', '2024-03-20 03:31:10', NULL);
INSERT INTO `departments` VALUES (18, 1, 2, 'Balintocatoc Integrated School', 1, 1, '2024-03-20 03:31:26', '2024-03-20 03:31:34', NULL);
INSERT INTO `departments` VALUES (19, 1, 2, 'Balaurte Elementary School', 1, 1, '2024-03-20 03:31:51', '2024-03-20 03:31:51', NULL);
INSERT INTO `departments` VALUES (20, 1, 2, 'Bannawag Norte Integrated School', 1, 1, '2024-03-20 03:32:13', '2024-03-20 03:32:13', NULL);
INSERT INTO `departments` VALUES (21, 1, 2, 'Calaocan Elementary School', 1, 1, '2024-03-20 03:32:29', '2024-03-20 03:32:29', NULL);
INSERT INTO `departments` VALUES (22, 1, 2, 'Luna Elementary School', 1, 1, '2024-03-20 03:32:48', '2024-03-20 03:32:48', NULL);
INSERT INTO `departments` VALUES (23, 1, 2, 'Rosario Elementary School', 1, 1, '2024-03-20 03:33:00', '2024-03-20 03:33:00', NULL);
INSERT INTO `departments` VALUES (24, 1, 2, 'San Jose Integrated School', 1, 1, '2024-03-20 03:33:12', '2024-03-20 03:33:12', NULL);
INSERT INTO `departments` VALUES (25, 1, 2, 'Santiago South Central School', 1, 1, '2024-03-20 03:33:28', '2024-03-20 03:33:28', NULL);
INSERT INTO `departments` VALUES (26, 1, 2, 'Sta Rosa Elementary School', 1, 1, '2024-03-20 03:33:43', '2024-03-20 03:33:43', NULL);
INSERT INTO `departments` VALUES (27, 1, 2, 'Dubinan Elementary School', 1, 1, '2024-03-20 03:33:59', '2024-03-20 03:33:59', NULL);
INSERT INTO `departments` VALUES (28, 1, 2, 'Patul Elementary School', 1, 1, '2024-03-20 03:34:09', '2024-03-20 03:34:09', NULL);
INSERT INTO `departments` VALUES (29, 1, 2, 'San Isidro Elementary School', 1, 1, '2024-03-20 03:34:29', '2024-03-20 03:34:29', NULL);
INSERT INTO `departments` VALUES (30, 1, 2, 'Sinili Integrated School', 1, 1, '2024-03-20 03:34:41', '2024-03-20 03:34:41', NULL);
INSERT INTO `departments` VALUES (31, 1, 2, 'Sinsayon Elementary School', 1, 1, '2024-03-20 03:34:56', '2024-03-20 03:34:56', NULL);
INSERT INTO `departments` VALUES (32, 1, 2, 'Santiago West Central School', 1, 1, '2024-03-20 03:35:10', '2024-03-20 03:35:10', NULL);
INSERT INTO `departments` VALUES (33, 1, 2, 'Villa Gonzaga Elementary School', 1, 1, '2024-03-20 03:35:26', '2024-03-20 03:35:26', NULL);
INSERT INTO `departments` VALUES (34, 1, 2, 'Cabulay High School', 1, 1, '2024-03-20 03:35:43', '2024-03-20 03:35:43', NULL);
INSERT INTO `departments` VALUES (35, 1, 2, 'Divisoria High School', 1, 1, '2024-03-20 03:35:57', '2024-03-20 03:35:57', NULL);
INSERT INTO `departments` VALUES (36, 1, 2, 'Naggasican National High School', 1, 1, '2024-03-20 03:36:15', '2024-03-20 03:36:24', NULL);
INSERT INTO `departments` VALUES (37, 1, 2, 'Patul National High School', 1, 1, '2024-03-20 03:36:59', '2024-03-20 03:36:59', NULL);
INSERT INTO `departments` VALUES (38, 1, 2, 'Rizal National High School', 1, 1, '2024-03-20 03:37:15', '2024-03-20 03:37:15', NULL);
INSERT INTO `departments` VALUES (39, 1, 2, 'Rosario National High School', 1, 1, '2024-03-20 03:37:30', '2024-03-20 03:37:30', NULL);
INSERT INTO `departments` VALUES (40, 1, 2, 'Santiago City National High School', 1, 1, '2024-03-20 03:37:45', '2024-03-20 03:37:45', NULL);
INSERT INTO `departments` VALUES (41, 1, 2, 'Sagana National High School', 1, 1, '2024-03-20 03:38:01', '2024-03-20 03:38:01', NULL);
INSERT INTO `departments` VALUES (42, 1, 2, 'Sinsayon National High School', 1, 1, '2024-03-20 03:38:15', '2024-03-20 03:38:15', NULL);
INSERT INTO `departments` VALUES (43, 1, 1, 'Information Communication Technology Unit', 1, 0, '2024-03-20 03:38:37', '2024-03-20 03:38:50', NULL);
INSERT INTO `departments` VALUES (44, 1, 1, 'Schools Division Superintendent', 1, 0, '2024-03-20 03:39:10', '2024-03-20 03:39:10', NULL);
INSERT INTO `departments` VALUES (45, 1, 1, 'Assistant Schools Division Superintendent', 1, 0, '2024-03-20 03:39:26', '2024-03-20 03:39:26', NULL);
INSERT INTO `departments` VALUES (46, 1, 1, 'Budget Unit', 1, 0, '2024-03-20 03:39:40', '2024-03-20 03:39:40', NULL);
INSERT INTO `departments` VALUES (47, 1, 1, 'Accounting Unit', 1, 0, '2024-03-20 03:39:57', '2024-03-20 03:39:57', NULL);
INSERT INTO `departments` VALUES (48, 1, 1, 'Cash Unit', 1, 0, '2024-03-20 03:40:09', '2024-03-20 03:40:09', NULL);
INSERT INTO `departments` VALUES (49, 1, 1, 'Administrative Unit', 1, 0, '2024-03-20 03:40:26', '2024-03-20 03:40:26', NULL);
INSERT INTO `departments` VALUES (50, 1, 1, 'Records Unit', 1, 0, '2024-03-20 03:40:37', '2024-03-20 03:40:37', NULL);
INSERT INTO `departments` VALUES (51, 1, 1, 'Curriculum Implementation Division', 1, 0, '2024-03-20 03:41:14', '2024-03-20 03:41:14', NULL);
INSERT INTO `departments` VALUES (52, 1, 1, 'Schools Governance And Operations Division', 1, 0, '2024-03-20 03:41:42', '2024-03-20 03:41:42', NULL);

-- ----------------------------
-- Table structure for elements
-- ----------------------------
DROP TABLE IF EXISTS `elements`;
CREATE TABLE `elements`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `assistance_id` bigint UNSIGNED NOT NULL,
  `nature_id` bigint UNSIGNED NOT NULL,
  `sub_type_id` bigint UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `elements to assistances`(`assistance_id` ASC) USING BTREE,
  INDEX `elements to natures`(`nature_id` ASC) USING BTREE,
  INDEX `elements to sub_types`(`sub_type_id` ASC) USING BTREE,
  CONSTRAINT `elements to assistances` FOREIGN KEY (`assistance_id`) REFERENCES `assistances` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `elements to natures` FOREIGN KEY (`nature_id`) REFERENCES `natures` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `elements to sub_types` FOREIGN KEY (`sub_type_id`) REFERENCES `sub_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of elements
-- ----------------------------

-- ----------------------------
-- Table structure for establishments
-- ----------------------------
DROP TABLE IF EXISTS `establishments`;
CREATE TABLE `establishments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `establishments to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `establishments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of establishments
-- ----------------------------
INSERT INTO `establishments` VALUES (1, 1, 'Office', 1, '2024-03-10 19:17:18', '2024-03-10 19:17:18', NULL);
INSERT INTO `establishments` VALUES (2, 1, 'School', 1, '2024-03-10 19:17:24', '2024-03-10 19:17:24', NULL);

-- ----------------------------
-- Table structure for findings
-- ----------------------------
DROP TABLE IF EXISTS `findings`;
CREATE TABLE `findings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `assistance_id` bigint UNSIGNED NOT NULL,
  `item_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `serial_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `problem_issue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `findings to assistances`(`assistance_id` ASC) USING BTREE,
  CONSTRAINT `findings to assistances` FOREIGN KEY (`assistance_id`) REFERENCES `assistances` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of findings
-- ----------------------------
INSERT INTO `findings` VALUES (1, 1, '', '', '', '2024-03-18 19:41:05', '2024-03-18 19:41:05', NULL);
INSERT INTO `findings` VALUES (2, 1, '', '', '', '2024-03-18 19:41:05', '2024-03-20 04:57:21', NULL);
INSERT INTO `findings` VALUES (3, 1, '', '', '', '2024-03-18 19:41:05', '2024-03-18 19:41:05', NULL);

-- ----------------------------
-- Table structure for heads
-- ----------------------------
DROP TABLE IF EXISTS `heads`;
CREATE TABLE `heads`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `primary_id` bigint UNSIGNED NOT NULL,
  `cluster_id` bigint UNSIGNED NULL DEFAULT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `head_contact_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `school_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ict_coordinator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ict_contact_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `heads to users`(`user_id` ASC) USING BTREE,
  INDEX `heads to primaries`(`primary_id` ASC) USING BTREE,
  INDEX `heads to clusters`(`cluster_id` ASC) USING BTREE,
  INDEX `heads to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `heads to clusters` FOREIGN KEY (`cluster_id`) REFERENCES `clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `heads to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `heads to primaries` FOREIGN KEY (`primary_id`) REFERENCES `primaries` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `heads to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of heads
-- ----------------------------
INSERT INTO `heads` VALUES (1, 1, 1, 1, 1, 'Floara', '09100575676', '103817', '2222', '09100575676', '2024-03-11 01:23:17', '2024-03-11 01:28:28', '2024-03-19 19:42:16');
INSERT INTO `heads` VALUES (2, 1, 1, 1, 9, 'FLORA C. MABALOT', '09054142117', '103823', 'ROWENA E. CALIBUSO', '09123456789', '2024-03-20 03:45:38', '2024-03-20 03:45:38', NULL);
INSERT INTO `heads` VALUES (3, 1, 1, 1, 10, 'MARIO P. MABALOT', '09168462494', '103824', 'MICHAEL I. TOMAS', '09123456789', '2024-03-20 03:46:46', '2024-03-20 03:46:57', NULL);
INSERT INTO `heads` VALUES (4, 1, 1, 3, 18, 'CONCHITA C. OBENA', '09491229624', '500936', 'EUGENE N. EUSEBIO', '09123456789', '2024-03-20 03:48:21', '2024-03-20 03:48:21', NULL);
INSERT INTO `heads` VALUES (5, 1, 1, 3, 19, 'BERNALDO S. VICENTE', '09175044998', '103835', 'CHARYCEL S. DE GUZMAN', '09175061192', '2024-03-20 03:49:47', '2024-03-20 03:49:47', NULL);
INSERT INTO `heads` VALUES (6, 1, 1, 3, 20, 'ROBELYN P. AGUINALDO', '09264491092', '103836', 'EDDIE R. DAVIN', '09123456789', '2024-03-20 03:51:32', '2024-03-20 03:51:32', NULL);
INSERT INTO `heads` VALUES (7, 1, 1, 1, 11, 'MARICRIS F. SALES', '09972948367', '103811', 'KARYL ANN A. MANANGAN', '09123456789', '2024-03-20 03:52:39', '2024-03-20 03:52:39', NULL);
INSERT INTO `heads` VALUES (8, 1, 1, 2, 2, 'LOLITA V. LORENZO', '09058208332', '103812', 'JAMES BRYAN V. RUIZ', '09123456789', '2024-03-20 03:53:55', '2024-03-20 03:53:55', NULL);
INSERT INTO `heads` VALUES (9, 1, 1, 1, 12, 'BRENDA NIEBRES', '09123456789', '103825', 'VICTOR B. TACAMA', '09123456789', '2024-03-20 03:56:10', '2024-03-20 03:56:10', NULL);
INSERT INTO `heads` VALUES (10, 1, 1, 1, 13, 'EDITHA V. BLAS', '09678068923', '103826', 'MARICEL N. PARANIS', '09123456789', '2024-03-20 03:57:32', '2024-03-20 03:57:32', NULL);
INSERT INTO `heads` VALUES (11, 1, 2, 1, 34, 'RUBELYN A CASTRO', '09175678955', '300505', 'TRIXIE G. SUMALBAG', '09123456789', '2024-03-20 03:58:55', '2024-03-20 03:58:55', NULL);
INSERT INTO `heads` VALUES (12, 1, 1, 3, 21, 'EDITHA C. DIMARUCUT', '09123456789', '103837', 'ELYSON S. PRESENTACION', '09123456789', '2024-03-20 04:00:30', '2024-03-20 04:00:30', NULL);
INSERT INTO `heads` VALUES (13, 1, 1, 2, 3, 'JOHNNY C. LALAS', '09277436790', '103813', 'JOSEPH C. CASTRO', '09123456789', '2024-03-20 04:01:41', '2024-03-20 04:01:41', NULL);
INSERT INTO `heads` VALUES (14, 1, 2, 2, 35, 'AUDITA W. CAMBOD', '09175074044', '300528', 'RODOLFO CUBANGBANG JR.', '09123456789', '2024-03-20 04:03:02', '2024-03-20 04:03:02', NULL);
INSERT INTO `heads` VALUES (15, 1, 1, 4, 27, 'MARJORIE D. PILON', '09152808374', '103827', 'ANGELIKA D. RESPONZO', '09123456789', '2024-03-20 04:04:14', '2024-03-20 04:04:14', NULL);
INSERT INTO `heads` VALUES (16, 1, 1, 3, 22, 'JOCELYN B. AGTONTON', '09236314780', '103814', 'ROVELYN O. TAMMIDAO', '09123456789', '2024-03-20 04:05:26', '2024-03-20 04:05:26', NULL);
INSERT INTO `heads` VALUES (17, 1, 1, 2, 4, 'ROSELILY M. ESTEBAN', '09171069072', '103815', 'CHRISTIAN DONNE S. HERNAL', '09123456789', '2024-03-20 04:06:37', '2024-03-20 04:06:37', NULL);
INSERT INTO `heads` VALUES (18, 1, 1, 1, 14, 'MACL VER G. LANNU', '09353548490', '103816', 'JERRY L. GAMIT', '09123456789', '2024-03-20 04:07:51', '2024-03-20 04:07:51', NULL);
INSERT INTO `heads` VALUES (19, 1, 1, 1, 15, 'MACL VER G. LANNU', '09353548490', '103817', 'FEDERICO L. ESCOVIDAL', '09123456789', '2024-03-20 04:09:09', '2024-03-20 04:09:09', NULL);
INSERT INTO `heads` VALUES (20, 1, 1, 2, 5, 'AMELIA B. BERGONIA', '09171078502', '103818', 'IAN P. AGUSTIN', '09123456789', '2024-03-20 04:10:16', '2024-03-20 04:10:16', NULL);
INSERT INTO `heads` VALUES (21, 1, 2, 2, 36, 'EMERITA T. MAWIRAT', '09056609709', '325204', 'ARIEL M. DELOS SANTOS', '09123456789', '2024-03-20 04:11:32', '2024-03-20 04:11:32', NULL);
INSERT INTO `heads` VALUES (22, 1, 1, 4, 28, 'MARILOU L. BUIZON', '09355404524', '103828', 'JINGKIE D. CABUGAO', '09123456789', '2024-03-20 04:12:39', '2024-03-20 04:12:39', NULL);
INSERT INTO `heads` VALUES (23, 1, 2, 4, 37, 'ELOISA L. DIZON', '09399384651', '306124', 'EMERSON R. RESPONZO', '09123456789', '2024-03-20 04:14:07', '2024-03-20 04:14:07', NULL);
INSERT INTO `heads` VALUES (24, 1, 2, 2, 38, 'ELMERCHITA B. RIBUCA', '09179804853', '300578-1', 'LEONARD B. SAMBILE', '09123456789', '2024-03-20 04:15:53', '2024-03-20 04:32:05', NULL);
INSERT INTO `heads` VALUES (25, 1, 1, 3, 23, 'LEONORA L. TUASON', '09269414787', '103838', 'JOANNA MARIE M. PERALTA', '09123456789', '2024-03-20 04:16:59', '2024-03-20 04:16:59', NULL);
INSERT INTO `heads` VALUES (26, 1, 2, 3, 39, 'MA. CRISTINA A. PAPA', '09062321966', '325203', 'RUBIE JOY R. RUSTIA', '09123456789', '2024-03-20 04:18:12', '2024-03-20 04:18:12', NULL);
INSERT INTO `heads` VALUES (27, 1, 1, 1, 16, 'JOCELYN B. AGTONTON', '09236314780', '103819', 'JAMELA A. PANTALEON', '09123456789', '2024-03-20 04:19:40', '2024-03-20 04:19:40', NULL);
INSERT INTO `heads` VALUES (28, 1, 2, 1, 41, 'ELSIE GRACE S. BATARA', '09663324439', '325202', 'MELZAM D. PAULINO', '09123456789', '2024-03-20 04:20:54', '2024-03-20 04:20:54', NULL);
INSERT INTO `heads` VALUES (29, 1, 2, 2, 6, 'RECHELYN B. CONSTANTINO', '09067011374', '500937', 'MARK JOSEPH C. SARMIENTO', '09123456789', '2024-03-20 04:23:39', '2024-03-20 04:23:39', NULL);
INSERT INTO `heads` VALUES (30, 1, 1, 2, 7, 'HILDA R. MARIANO', '09175073246', '103821', 'MERCHAN M. QUINTOS', '09123456789', '2024-03-20 04:25:07', '2024-03-20 04:25:07', NULL);
INSERT INTO `heads` VALUES (31, 1, 1, 4, 29, 'MICHAEL Y. TORRES', '09123456789', '103829', 'NICHOLLE ARIANNE C. DAQUIOAG', '09123456789', '2024-03-20 04:26:19', '2024-03-20 04:26:19', NULL);
INSERT INTO `heads` VALUES (32, 1, 1, 3, 24, 'JULIUS F. RAMOS', '09123456789', '501147', 'ELLAIZA MAE G. GABRIEL', '09123456789', '2024-03-20 04:27:18', '2024-03-20 04:27:40', NULL);
INSERT INTO `heads` VALUES (33, 1, 2, 3, 40, 'RANDY B. MINA', '09171241809', '300578', 'RAFFY A. URSULUM', '09123456789', '2024-03-20 04:33:06', '2024-03-20 04:33:06', NULL);
INSERT INTO `heads` VALUES (34, 1, 1, 2, 8, 'CORAZON A. FERRER', '09278760560', '103822', 'JESSA E. LABUGUEN', '09123456789', '2024-03-20 04:34:59', '2024-03-20 04:34:59', NULL);
INSERT INTO `heads` VALUES (35, 1, 1, 1, 17, 'MELOZAR F. DIZON', '09151992303', '103830', 'MAILA M. DULAWAN', '09123456789', '2024-03-20 04:35:59', '2024-03-20 04:35:59', NULL);
INSERT INTO `heads` VALUES (36, 1, 1, 3, 25, 'JOY C. GABRIEL', '09263798756', '103840', 'CHRISTIAN LLOYD B. GORDO', '09123456789', '2024-03-20 04:37:11', '2024-03-20 04:37:11', NULL);
INSERT INTO `heads` VALUES (37, 1, 1, 4, 32, 'OFELIA M. LENGOYNA', '09771768098', '103842', 'ROLEEN MIZZY D. PERLADO', '09123456789', '2024-03-20 04:38:20', '2024-03-20 04:38:20', NULL);
INSERT INTO `heads` VALUES (38, 1, 1, 4, 30, 'JEFFREY C. GABRIEL', '09171533860', '500938', 'ARIANE ACE T. DE GUZMAN', '09123456789', '2024-03-20 04:39:21', '2024-03-20 04:39:21', NULL);
INSERT INTO `heads` VALUES (39, 1, 1, 4, 31, 'ALFREDO R. DAHAN', '09560565213', '103832', 'CHRISTOPHER V. BAUTISTA', '09123456789', '2024-03-20 04:40:31', '2024-03-20 04:40:31', NULL);
INSERT INTO `heads` VALUES (40, 1, 2, 4, 42, 'JOEL S. DOMINGO', '09265920364', '325201', 'CHRISTIAN ESTEBAN', '09123456789', '2024-03-20 04:41:42', '2024-03-20 04:41:42', NULL);
INSERT INTO `heads` VALUES (41, 1, 1, 3, 26, 'JULIUS F. RAMOS', '09123456789', '103841', 'JUNALEN K. MANZANO', '09123456789', '2024-03-20 04:42:27', '2024-03-20 04:42:27', NULL);
INSERT INTO `heads` VALUES (42, 1, 1, 4, 33, 'ALLEN D. MANUEL', '09056931316', '103833', 'REALYN R. PASCUAL', '09123456789', '2024-03-20 04:43:28', '2024-03-20 04:43:28', NULL);

-- ----------------------------
-- Table structure for natures
-- ----------------------------
DROP TABLE IF EXISTS `natures`;
CREATE TABLE `natures`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `nature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `natures to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `natures to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of natures
-- ----------------------------
INSERT INTO `natures` VALUES (1, 1, 'HARDWARE', 1, 2, '2024-03-11 18:10:51', '2024-03-11 18:10:51', '2024-03-19 19:17:58');
INSERT INTO `natures` VALUES (2, 1, 'SOFTWARE', 1, 1, '2024-03-11 18:11:02', '2024-03-11 19:46:00', '2024-03-19 19:17:54');
INSERT INTO `natures` VALUES (3, 1, 'HARDWARE', 1, 1, '2024-03-20 03:18:32', '2024-03-20 03:18:32', NULL);
INSERT INTO `natures` VALUES (4, 1, 'SOFTWARE', 1, 2, '2024-03-20 03:18:40', '2024-03-20 03:18:40', NULL);
INSERT INTO `natures` VALUES (5, 1, 'NETWORK', 1, 3, '2024-03-20 03:18:47', '2024-03-20 03:18:47', NULL);
INSERT INTO `natures` VALUES (6, 1, 'OTHERS', 1, 4, '2024-03-20 03:18:53', '2024-03-20 03:18:53', NULL);

-- ----------------------------
-- Table structure for primaries
-- ----------------------------
DROP TABLE IF EXISTS `primaries`;
CREATE TABLE `primaries`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `primary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `primaries to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `primaries to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of primaries
-- ----------------------------
INSERT INTO `primaries` VALUES (1, 1, 'Primary', 1, '2024-03-11 00:03:55', '2024-03-11 00:03:55', NULL);
INSERT INTO `primaries` VALUES (2, 1, 'Secondary', 1, '2024-03-11 00:04:26', '2024-03-11 00:06:32', NULL);

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-03-10 11:14:14', '2024-03-10 11:14:14', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (3, '2024-03-11 23:39:21', '2024-03-11 23:39:21', 'Users', '1', '73e687ac0584fe91ac61cf8cb000c8bee00a97ea', 'db96c6483e502da184e6a77fc48ac28e5a4686d4', '2024-04-10 23:39:21');
INSERT INTO `remember_me_tokens` VALUES (6, '2024-03-20 03:03:36', '2024-03-20 03:03:36', 'Users', '1', '1e5015a8d55dba22660b6ddb0f31e51ccbe3364f', '3c2a80f0bb3efeb7f0b888dd0ddc48682e4c154b', '2024-04-19 03:03:36');
INSERT INTO `remember_me_tokens` VALUES (7, '2024-03-22 17:23:15', '2024-03-23 16:57:52', 'Users', '1', '33f6ce0f83d7d40dce5ea244a1f6631e1c3c5f18', '64aebede2aa2d6513f8e705bae68f3e0f1c84ea6', '2024-04-22 16:57:51');
INSERT INTO `remember_me_tokens` VALUES (8, '2024-03-24 22:35:59', '2024-03-24 22:35:59', 'Users', '1', '9b98731e0a7d4c6f12849edf653d486c3424cd3c', '72b7af4d1abaee59ca9dc6455e271b8a622dc66f', '2024-04-23 22:35:58');
INSERT INTO `remember_me_tokens` VALUES (10, '2024-04-08 17:47:31', '2024-04-08 17:47:31', 'Users', '1', '037d1146d8ddf6a6c4df4d385c1f4f24c0d1ec35', '67a97e4bc5c8bc03df7f740152fe84c891745666', '2024-05-08 17:47:31');
INSERT INTO `remember_me_tokens` VALUES (11, '2024-04-10 22:17:56', '2024-04-10 22:17:56', 'Users', '1', '32cde1d7d89ac5d58c21cd7bf8c57d53fef77591', 'f0235468299dc1c6cfe246d7ee39f0d934bb6d12', '2024-05-10 22:17:56');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `account_id` bigint UNSIGNED NOT NULL,
  `request` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `is_default` tinyint NOT NULL DEFAULT 0,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests to users`(`user_id` ASC) USING BTREE,
  INDEX `requests to accounts`(`account_id` ASC) USING BTREE,
  CONSTRAINT `requests to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES (1, 1, 8, 'Create', 1, 0, 1, '2024-03-11 23:49:40', '2024-03-11 23:49:40', NULL);
INSERT INTO `requests` VALUES (2, 1, 8, 'Reset', 1, 0, 5, '2024-03-11 23:57:44', '2024-03-20 04:47:42', NULL);
INSERT INTO `requests` VALUES (3, 1, 8, 'Update', 1, 0, 2, '2024-03-20 04:47:53', '2024-03-20 04:47:53', NULL);
INSERT INTO `requests` VALUES (4, 1, 8, 'Suspend', 1, 0, 3, '2024-03-20 04:48:01', '2024-03-20 04:48:01', NULL);
INSERT INTO `requests` VALUES (5, 1, 8, 'Delete', 1, 0, 4, '2024-03-20 04:48:11', '2024-03-20 04:48:11', NULL);
INSERT INTO `requests` VALUES (6, 1, 9, 'Create', 1, 0, 1, '2024-03-20 04:49:38', '2024-03-20 04:49:38', NULL);
INSERT INTO `requests` VALUES (7, 1, 9, 'Reset', 1, 0, 2, '2024-03-20 04:49:46', '2024-03-20 04:49:46', NULL);
INSERT INTO `requests` VALUES (8, 1, 9, 'Update', 1, 0, 3, '2024-03-20 04:49:55', '2024-03-20 04:49:55', NULL);
INSERT INTO `requests` VALUES (9, 1, 9, 'Suspend', 1, 0, 4, '2024-03-20 04:50:12', '2024-03-20 04:50:12', NULL);
INSERT INTO `requests` VALUES (10, 1, 9, 'Delete', 1, 0, 5, '2024-03-20 04:50:21', '2024-03-20 04:50:21', NULL);
INSERT INTO `requests` VALUES (11, 1, 10, 'Create', 1, 0, 1, '2024-03-20 04:50:35', '2024-03-20 04:50:35', NULL);
INSERT INTO `requests` VALUES (12, 1, 10, 'Reset', 1, 0, 2, '2024-03-20 04:50:44', '2024-03-20 04:50:44', NULL);
INSERT INTO `requests` VALUES (13, 1, 10, 'Update', 1, 0, 3, '2024-03-20 04:51:18', '2024-03-20 04:51:18', NULL);
INSERT INTO `requests` VALUES (14, 1, 10, 'Suspend', 1, 0, 4, '2024-03-20 04:51:28', '2024-03-20 04:51:28', NULL);
INSERT INTO `requests` VALUES (15, 1, 10, 'Delete', 1, 0, 5, '2024-03-20 04:51:36', '2024-03-20 04:51:36', NULL);
INSERT INTO `requests` VALUES (16, 1, 11, 'Create', 1, 0, 1, '2024-03-20 04:51:45', '2024-03-20 04:51:45', NULL);
INSERT INTO `requests` VALUES (17, 1, 11, 'Reset', 1, 0, 2, '2024-03-20 04:51:53', '2024-03-20 04:51:53', NULL);
INSERT INTO `requests` VALUES (18, 1, 11, 'Update', 1, 0, 3, '2024-03-20 04:51:59', '2024-03-20 04:51:59', NULL);
INSERT INTO `requests` VALUES (19, 1, 11, 'Suspend', 1, 0, 4, '2024-03-20 04:52:08', '2024-03-20 04:52:08', NULL);
INSERT INTO `requests` VALUES (20, 1, 11, 'Delete', 1, 0, 5, '2024-03-20 04:52:14', '2024-03-20 04:52:14', NULL);
INSERT INTO `requests` VALUES (21, 1, 7, 'Hardware', 1, 0, 1, '2024-03-20 04:52:56', '2024-03-20 04:52:56', NULL);
INSERT INTO `requests` VALUES (22, 1, 7, 'Software', 1, 0, 2, '2024-03-20 04:53:09', '2024-03-20 04:53:09', NULL);
INSERT INTO `requests` VALUES (23, 1, 7, 'Network', 1, 0, 3, '2024-03-20 04:53:20', '2024-03-20 04:53:20', NULL);
INSERT INTO `requests` VALUES (24, 1, 7, 'Others', 1, 0, 4, '2024-03-20 04:53:27', '2024-03-20 04:53:27', NULL);

-- ----------------------------
-- Table structure for sub_types
-- ----------------------------
DROP TABLE IF EXISTS `sub_types`;
CREATE TABLE `sub_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `nature_id` bigint UNSIGNED NOT NULL,
  `sub_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sub_types to users`(`user_id` ASC) USING BTREE,
  INDEX `sub_types to natures`(`nature_id` ASC) USING BTREE,
  CONSTRAINT `sub_types to natures` FOREIGN KEY (`nature_id`) REFERENCES `natures` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sub_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sub_types
-- ----------------------------
INSERT INTO `sub_types` VALUES (3, 1, 3, 'Printer', 1, 1, '2024-03-20 03:19:10', '2024-03-20 03:19:10', NULL);
INSERT INTO `sub_types` VALUES (4, 1, 3, 'System Unit', 1, 2, '2024-03-20 03:19:18', '2024-03-20 03:19:18', NULL);
INSERT INTO `sub_types` VALUES (5, 1, 3, 'Monitor/Display', 1, 3, '2024-03-20 03:19:29', '2024-03-20 03:19:29', NULL);
INSERT INTO `sub_types` VALUES (6, 1, 3, 'Internal', 1, 4, '2024-03-20 03:19:38', '2024-03-20 03:19:38', NULL);
INSERT INTO `sub_types` VALUES (7, 1, 3, 'Peripherals', 1, 5, '2024-03-20 03:19:46', '2024-03-20 03:19:46', NULL);
INSERT INTO `sub_types` VALUES (8, 1, 3, 'Connectors/Plugs/Power', 1, 6, '2024-03-20 03:20:08', '2024-03-20 03:20:08', NULL);
INSERT INTO `sub_types` VALUES (9, 1, 4, 'OS', 1, 1, '2024-03-20 03:20:18', '2024-03-20 03:20:18', NULL);
INSERT INTO `sub_types` VALUES (10, 1, 4, 'Drivers', 1, 2, '2024-03-20 03:20:26', '2024-03-20 03:20:26', NULL);
INSERT INTO `sub_types` VALUES (11, 1, 4, 'Malware', 1, 3, '2024-03-20 03:20:34', '2024-03-20 03:20:34', NULL);
INSERT INTO `sub_types` VALUES (12, 1, 4, 'Installation', 1, 4, '2024-03-20 03:20:56', '2024-03-20 03:20:56', NULL);
INSERT INTO `sub_types` VALUES (13, 1, 4, 'Update', 1, 5, '2024-03-20 03:21:05', '2024-03-20 03:21:05', NULL);
INSERT INTO `sub_types` VALUES (14, 1, 4, 'Files/Data', 1, 6, '2024-03-20 03:21:15', '2024-03-20 03:21:15', NULL);
INSERT INTO `sub_types` VALUES (15, 1, 5, 'LAN Configuration', 1, 1, '2024-03-20 03:21:27', '2024-03-21 23:36:46', NULL);
INSERT INTO `sub_types` VALUES (16, 1, 5, 'Router/Cables', 1, 2, '2024-03-20 03:21:42', '2024-03-20 03:21:42', NULL);
INSERT INTO `sub_types` VALUES (17, 1, 5, 'Internet', 1, 3, '2024-03-20 03:21:52', '2024-03-20 03:21:52', NULL);
INSERT INTO `sub_types` VALUES (18, 1, 6, 'DCP', 1, 1, '2024-03-20 03:22:10', '2024-03-20 03:22:10', NULL);

-- ----------------------------
-- Table structure for takens
-- ----------------------------
DROP TABLE IF EXISTS `takens`;
CREATE TABLE `takens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `assistance_id` bigint UNSIGNED NOT NULL,
  `action_id` bigint UNSIGNED NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `takens to assistances`(`assistance_id` ASC) USING BTREE,
  INDEX `takens to actions`(`action_id` ASC) USING BTREE,
  CONSTRAINT `takens to actions` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `takens to assistances` FOREIGN KEY (`assistance_id`) REFERENCES `assistances` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of takens
-- ----------------------------
INSERT INTO `takens` VALUES (43, 1, 2, '2024-03-26 22:52:12', '2024-03-26 22:52:12', NULL);
INSERT INTO `takens` VALUES (44, 1, 5, '2024-03-26 22:52:12', '2024-03-26 22:52:12', NULL);
INSERT INTO `takens` VALUES (45, 1, 3, '2024-03-26 22:52:12', '2024-03-26 22:52:12', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$VhFe7RfVIYZy0NsM8TMO5eH6WxiWsCJZ1YqPD/YGz.FFupjKlVDLy', '66162f151381d66162f1513822', '2024-02-25 00:21:50', '2024-04-10 22:17:57', NULL);
INSERT INTO `users` VALUES (2, 'steven', 'shangqt', 'steveror90@gmail.com', '$2y$10$OKarulllyUBcSNG0hoUMLu1dVXb.LA7sXm3IXOBFuWFT5Kfhzjp8S', '65ebf977eb236', '2024-03-09 21:53:54', '2024-03-09 21:54:02', NULL);

SET FOREIGN_KEY_CHECKS = 1;
